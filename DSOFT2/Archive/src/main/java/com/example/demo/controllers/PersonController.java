package com.example.demo.controllers;

import java.util.List;
import java.util.Optional;

import com.example.demo.DTO.PersonDTO;
import com.example.demo.domain.valueobjects.PersonId;
import com.example.demo.services.IServices.IPersonService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.example.demo.controllers.IControllers.IPersonController;

@Controller
public class PersonController implements IPersonController {

    private static final Logger LOG = LoggerFactory.getLogger(PersonController.class);

    @Autowired
    private final IPersonService personService;

    @Autowired
    public PersonController(IPersonService personService) {
        this.personService = personService;
    }

    public void newPerson(long id, String firstName, String lastName) {
        LOG.info("<<Create a Person>>");

        personService.createAndSavePerson(id, firstName, lastName);
    }

    public List<PersonDTO> findAllPersons() {
        return personService.findAll();
    }

    public Optional<PersonDTO> findPersonById(long id) {
        // fetch an individual person by ID
        PersonId personId = new PersonId(id);
        Optional<PersonDTO> opPerson = personService.findById(personId);

        return opPerson;
    }
    
}