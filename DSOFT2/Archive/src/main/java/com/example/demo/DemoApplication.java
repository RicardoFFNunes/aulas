package com.example.demo;

import java.util.Optional;

import com.example.demo.DTO.PersonDTO;
import com.example.demo.controllers.PersonController;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
public class DemoApplication {

	private static final Logger log = LoggerFactory.getLogger(DemoApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo( PersonController personController ) {
		return (args) -> {

			log.info("Hello!");

			personController.newPerson(1L, "firstName", "lastName");
			personController.newPerson(2L, "firstName2", "lastName2");

			log.info("Persons created!");

			log.info("All Persons: " + personController.findAllPersons());

			Optional<PersonDTO> opPerson = personController.findPersonById(1L);

			if( opPerson.isPresent() ) {
				PersonDTO person = opPerson.get();
				log.info("Person found with findById(1L):");
				log.info("--------------------------------");
				log.info(person.toString());
				log.info("");
			}

		};
	}
}