package com.example.demo.services.IServices;

import java.util.List;
import java.util.Optional;

import com.example.demo.DTO.PersonDTO;
import com.example.demo.domain.valueobjects.PersonId;

public interface IPersonService {
    public PersonDTO createAndSavePerson(long id, String firstName, String lastName );
	public Optional<PersonDTO> findById(PersonId id);
	public List<PersonDTO> findAll();
}
