package com.example.demo.controllers.IControllers;

import java.util.List;
import java.util.Optional;

import com.example.demo.DTO.PersonDTO;

public interface IPersonController {
    public void newPerson(long id, String firstName, String lastName);
    public List<PersonDTO> findAllPersons();
    public Optional<PersonDTO> findPersonById(long id);
}
