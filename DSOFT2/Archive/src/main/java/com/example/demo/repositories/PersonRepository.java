package com.example.demo.repositories;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Optional;
import java.util.List;
import java.util.Map;

import com.example.demo.domain.entities.Person;
import com.example.demo.domain.valueobjects.PersonId;
import com.example.demo.services.IRepositories.IPersonRepository;

import org.springframework.stereotype.Repository;

@Repository
public class PersonRepository implements IPersonRepository {

	private Map<PersonId, Person> persons = new HashMap<PersonId,Person>();

	public Person save( Person person ) {

		persons.put(person.getId(), person);
		return person;
	}

	public Optional<Person> findById(PersonId id) {
		Person person = persons.get(id);
		
		return Optional.ofNullable(person);
	}

	public List<Person> findAll() {
		List<Person> personList = new LinkedList<Person>();
		personList.addAll(persons.values());

		return personList;
	}
}