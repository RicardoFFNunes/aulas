package com.example.demo.domain.entities;

import com.example.demo.domain.valueobjects.PersonId;

import lombok.Getter;
import lombok.ToString;

@ToString

public class Person {
	@Getter
	private final PersonId id;
	@Getter
	private String firstName;
	@Getter
	private String lastName;

	public Person(long id, String firstName, String lastName) {
		this.id = new PersonId(id);

		this.firstName = firstName;
		this.lastName = lastName;
	}

	public Person(PersonId id, String firstName, String lastName) {
		this.id = id;

		this.firstName = firstName;
		this.lastName = lastName;
	}
}
