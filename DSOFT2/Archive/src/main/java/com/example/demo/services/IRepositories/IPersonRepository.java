package com.example.demo.services.IRepositories;

import java.util.List;
import java.util.Optional;

import com.example.demo.domain.entities.Person;
import com.example.demo.domain.valueobjects.PersonId;

public interface IPersonRepository {
	public Person save( Person person );
	public Optional<Person> findById(PersonId id);
	public List<Person> findAll();
}
