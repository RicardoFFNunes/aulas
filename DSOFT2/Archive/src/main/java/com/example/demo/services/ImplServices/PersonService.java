package com.example.demo.services.ImplServices;

import java.util.ArrayList;
import java.util.Optional;
import java.util.List;

import com.example.demo.domain.entities.Person;
import com.example.demo.domain.valueobjects.PersonId;
import com.example.demo.DTO.PersonDTO;
import com.example.demo.DTO.assemblers.PersonDomainDTOAssembler;
import com.example.demo.services.IRepositories.IPersonRepository;
import com.example.demo.services.IServices.IPersonService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonService implements IPersonService {
    @Autowired
    private IPersonRepository personRepository;
    @Autowired
    private PersonDomainDTOAssembler personAssembler;

    public PersonService() {
    }

    public PersonDTO createAndSavePerson(long id, String firstName, String lastName ) {

        Person newPerson = new Person(id, firstName, lastName);

        personRepository.save(newPerson);

        PersonDTO personDTO = personAssembler.toDTO(newPerson.getId(), newPerson.getFirstName(), newPerson.getLastName());
        
        return personDTO;
    }

    public Optional<PersonDTO> findById(PersonId id) {
        
        Optional<Person> opPerson = personRepository.findById(id);

        if( opPerson.isPresent() ) {
            Person person = opPerson.get();
            PersonDTO personDTO = personAssembler.toDTO(person.getId(), person.getFirstName(), person.getLastName());
            return Optional.of(personDTO);
        }

        return Optional.empty();
    }

    public List<PersonDTO> findAll() {
        
        List<Person> setPerson = personRepository.findAll();

        List<PersonDTO> setPersonDTO = new ArrayList<PersonDTO>();
        for(Person person : setPerson ) {
            PersonDTO personDTO = personAssembler.toDTO(person.getId(), person.getFirstName(), person.getLastName());
            setPersonDTO.add(personDTO);
        }

        return setPersonDTO;
    }
}
