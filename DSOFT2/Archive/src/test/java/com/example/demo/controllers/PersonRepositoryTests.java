package com.example.demo.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import com.example.demo.DTO.PersonDTO;
import com.example.demo.domain.valueobjects.PersonId;
import com.example.demo.services.IServices.IPersonService;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonRepositoryTests {
	@Autowired
	private IPersonService personService;

	@Test
	public void testFindByLastName() {

		Long id = 1L;
		String firstName = "first";
		String lastName = "last";
		
		personService.createAndSavePerson(id, firstName, lastName);

		PersonId personId = new PersonId(id);
		Optional<PersonDTO> opPerson = personService.findById(personId);

		if( opPerson.isPresent() ) {
			PersonDTO personDTO = opPerson.get();

			assertEquals(personDTO.getId(), new PersonId(id));
			assertEquals(personDTO.getFirstName(), firstName);
			assertEquals(personDTO.getLastName(), lastName);
		}
	}
}
	