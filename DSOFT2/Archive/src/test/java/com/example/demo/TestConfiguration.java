package com.example.demo;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import com.example.demo.controllers.PersonController;

@Profile("test")
@Configuration
public class TestConfiguration {
	
	@Bean
	@Primary
	public PersonController PersonController() {
		return Mockito.mock(PersonController.class);
	}
}
