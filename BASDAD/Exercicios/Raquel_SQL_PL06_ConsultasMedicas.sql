--eliminar tabelas (eventualmente) existentes
DROP TABLE codigos_postais          CASCADE CONSTRAINTS PURGE;
DROP TABLE especialidades           CASCADE CONSTRAINTS PURGE;
DROP TABLE medicamentos             CASCADE CONSTRAINTS PURGE;
DROP TABLE medicos                  CASCADE CONSTRAINTS PURGE;
DROP TABLE pacientes                CASCADE CONSTRAINTS PURGE;
DROP TABLE consultas                CASCADE CONSTRAINTS PURGE;
DROP TABLE medicamentos_receitados  CASCADE CONSTRAINTS PURGE;

--criar tabelas
CREATE TABLE especialidades(
    id_especialidade INTEGER       CONSTRAINT pk_especialidades_id_especialidade PRIMARY KEY,
    designacao       VARCHAR2(20)  CONSTRAINT nn_especialidades_designacao       NOT NULL
);
  
CREATE TABLE medicos(
    id_medico         INTEGER       CONSTRAINT pk_medicos_id_medico         PRIMARY KEY,
    id_especialidade  INTEGER       CONSTRAINT nn_medicos_id_especialidade  NOT NULL,
    id_medico_chefe   INTEGER,       
    nome              VARCHAR2(20)  CONSTRAINT nn_medicos_nome              NOT NULL,
    hospital          VARCHAR2(20),

    data_nascimento   DATE          CONSTRAINT nn_medicos_data_nascimento   NOT NULL,
    cod_postal        VARCHAR(8)
    
);
  
CREATE TABLE pacientes(
    id_paciente     INTEGER         CONSTRAINT pk_pacientes_id_paciente       PRIMARY KEY,
    nome            VARCHAR2(20)    CONSTRAINT nn_pacientes_nome              NOT NULL,
    data_nascimento DATE            CONSTRAINT nn_pacientes_data_nascimento   NOT NULL,
    cod_postal      VARCHAR(8)
);
  
CREATE TABLE consultas(
    id_consulta INTEGER       CONSTRAINT pk_consultas_id_consultas  PRIMARY KEY,
    data_hora   TIMESTAMP     CONSTRAINT nn_consultas_data_hora     NOT NULL,
    id_medico   INTEGER       CONSTRAINT nn_consultas_id_medico     NOT NULL,
    id_paciente INTEGER       CONSTRAINT nn_consultas_id_paciente   NOT NULL,
    CONSTRAINT uk_consultas_data_hora_id_medico UNIQUE (data_hora,id_medico)
);

CREATE TABLE medicamentos(
    id_medicamento   INTEGER       CONSTRAINT pk_medicamentos_id_medicamento PRIMARY KEY,
    nome             VARCHAR2(20)  CONSTRAINT nn_medicamentos_nome           NOT NULL,
    laboratorio      VARCHAR2(20)  CONSTRAINT nn_medicamentos_laboratorio    NOT NULL
);
  
CREATE TABLE medicamentos_receitados(
    id_consulta    INTEGER,
    id_medicamento INTEGER,
    CONSTRAINT pk_medicamentos_receitas_id_consulta_id_medicamento PRIMARY KEY (id_consulta, id_medicamento)
);

CREATE TABLE codigos_postais(
    cod_postal  VARCHAR(8)     CONSTRAINT pk_codigos_postais_cod_postal PRIMARY KEY
                               CONSTRAINT ck_codigos_postais_cod_postal CHECK( REGEXP_LIKE(cod_postal,'\d{4}-\d{3}') ),         
    localidade  VARCHAR2(20)   CONSTRAINT nn_codigos_postais_localidade NOT NULL    
);

-- chaves estrangeiras
  
ALTER TABLE medicos ADD CONSTRAINT fk_medicos_cod_postal       
                            FOREIGN KEY (cod_postal) REFERENCES codigos_postais (cod_postal);
ALTER TABLE medicos ADD CONSTRAINT fk_medicos_id_especialidade 
                            FOREIGN KEY (id_especialidade) REFERENCES especialidades (id_especialidade);
ALTER TABLE medicos ADD CONSTRAINT fk_medicos_id_medico_chefe       
                            FOREIGN KEY (id_medico_chefe) REFERENCES medicos (id_medico);

ALTER TABLE medicamentos_receitados ADD CONSTRAINT fk_medicamentos_receitados_id_consultas   
                                            FOREIGN KEY (id_consulta) REFERENCES consultas (id_consulta);
ALTER TABLE medicamentos_receitados ADD CONSTRAINT fk_medicamentos_receitados_id_medicamento 
                                            FOREIGN KEY (id_medicamento) REFERENCES medicamentos (id_medicamento);

ALTER TABLE pacientes ADD CONSTRAINT fk_pacientes_cod_postal FOREIGN KEY (cod_postal)  REFERENCES codigos_postais (cod_postal);

ALTER TABLE consultas ADD CONSTRAINT fk_consultas_id_medico  FOREIGN KEY (id_medico)   REFERENCES medicos (id_medico);
ALTER TABLE consultas ADD CONSTRAINT fk_consultas_paciente   FOREIGN KEY (id_paciente) REFERENCES pacientes (id_paciente);

--preencher tabelas

-- c?digos postais
INSERT INTO codigos_postais VALUES ('2580-631', 'Alenquer');
INSERT INTO codigos_postais VALUES ('2720-465', 'Alverca');
INSERT INTO codigos_postais VALUES ('4600-012', 'Amarante');
INSERT INTO codigos_postais VALUES ('4750-264', 'Barcelos');
INSERT INTO codigos_postais VALUES ('2750-040', 'Cascais');
INSERT INTO codigos_postais VALUES ('4445-622', 'Ermesinde');
INSERT INTO codigos_postais VALUES ('4420-584', 'Gondomar');
INSERT INTO codigos_postais VALUES ('4420-282', 'Gondomar');
INSERT INTO codigos_postais VALUES ('1500-103', 'Lisboa');
INSERT INTO codigos_postais VALUES ('1099-052', 'Lisboa');
INSERT INTO codigos_postais VALUES ('1070-204', 'Lisboa');
INSERT INTO codigos_postais VALUES ('4470-208', 'Maia');
INSERT INTO codigos_postais VALUES ('4450-718', 'Matosinhos');
INSERT INTO codigos_postais VALUES ('4450-227', 'Matosinhos');
INSERT INTO codigos_postais VALUES ('4454-510', 'Matosinhos');
INSERT INTO codigos_postais VALUES ('2781-902', 'Oeiras');
INSERT INTO codigos_postais VALUES ('4200-197', 'Porto');
INSERT INTO codigos_postais VALUES ('4100-079', 'Porto');
INSERT INTO codigos_postais VALUES ('4100-217', 'Porto');
INSERT INTO codigos_postais VALUES ('4200-058', 'Porto');
INSERT INTO codigos_postais VALUES ('4150-706', 'Porto');
INSERT INTO codigos_postais VALUES ('4350-043', 'Porto');
INSERT INTO codigos_postais VALUES ('4050-078', 'Porto');
INSERT INTO codigos_postais VALUES ('4490-567', 'P?voa do Varzim');
INSERT INTO codigos_postais VALUES ('4460-393', 'Senhora da Hora');
INSERT INTO codigos_postais VALUES ('4440-501', 'Valongo');
INSERT INTO codigos_postais VALUES ('4481-908', 'Vila do Conde');
INSERT INTO codigos_postais VALUES ('4400-356', 'Vila Nova de Gaia');

-- especialidades
INSERT INTO especialidades VALUES (1, 'Pediatria');        
INSERT INTO especialidades VALUES (2, 'Cardiologia');      
INSERT INTO especialidades VALUES (3, 'Oftalmologia');     
INSERT INTO especialidades VALUES (4, 'Dermotologia');     

-- pacientes
INSERT INTO pacientes VALUES (1, 'Alfredo Gama',    TO_DATE('12-08-2003','dd-mm-yyyy'), '4454-510');
INSERT INTO pacientes VALUES (2, 'Cec�lia Mendes',  TO_DATE('31-10-2007','dd-mm-yyyy'), '4460-393');
INSERT INTO pacientes VALUES (3, 'Carolina Santos', TO_DATE('26-05-2013','dd-mm-yyyy'), '4460-393');
INSERT INTO pacientes VALUES (4, 'Daniela Seabra',  TO_DATE('05-06-2013','dd-mm-yyyy'), '4454-510');
INSERT INTO pacientes VALUES (5, 'Laura Barbosa',   TO_DATE('07-07-2000','dd-mm-yyyy'), '2750-040');
INSERT INTO pacientes VALUES (6, 'Paulo Barbosa',   TO_DATE('13-02-1953','dd-mm-yyyy'), '4460-393');

-- m?dicos chefe
INSERT INTO medicos VALUES (1, 2, NULL, 'Abel Santos',   'Pedro Hispano', TO_DATE('05-09-1973','dd-mm-yyyy'), '4200-197');
INSERT INTO medicos VALUES (2, 1, NULL, 'Adriana Sousa', 'Pedro Hispano', TO_DATE('23-03-1970','dd-mm-yyyy'), '4481-908');
INSERT INTO medicos VALUES (3, 4, NULL, 'Adriano Reis',  'Pedro Hispano', TO_DATE('07-07-1963','dd-mm-yyyy'), '4445-622');
INSERT INTO medicos VALUES (4, 3, NULL, 'Carla Dias',    'Pedro Hispano', TO_DATE('28-02-1960','dd-mm-yyyy'), '4460-393');

INSERT INTO medicos VALUES (5, 2, NULL, 'Ant�nio Coelho',   'Santa Maria', TO_DATE('02-02-1976','dd-mm-yyyy'), '1500-103');
INSERT INTO medicos VALUES (6, 1, NULL, 'Alvaro Dunas',     'Santa Maria', TO_DATE('15-06-1970','dd-mm-yyyy'), '1099-052');
INSERT INTO medicos VALUES (7, 4, NULL, 'Manuela Silva',    'Santa Maria', TO_DATE('01-01-1958','dd-mm-yyyy'), '1070-204');
INSERT INTO medicos VALUES (8, 3, NULL, 'Ant�nio Oliveira', 'Santa Maria', TO_DATE('16-04-1956','dd-mm-yyyy'), '2580-631');

INSERT INTO medicos VALUES (9,  2, NULL, 'Catarina Dolores',  'Santo Ant�nio', TO_DATE('09-11-1954','dd-mm-yyyy'), '4100-079');
INSERT INTO medicos VALUES (10, 1, NULL, 'Benjamim Mateus',   'Santo Ant�nio', TO_DATE('12-12-1958','dd-mm-yyyy'), '4100-217');
INSERT INTO medicos VALUES (11, 4, NULL, 'Francisco Cardoso', 'Santo Ant�nio', TO_DATE('11-09-1958','dd-mm-yyyy'), '4450-718');
INSERT INTO medicos VALUES (12, 3, NULL, 'D�bora Brand?o',    'Santo Ant�nio', TO_DATE('05-06-1961','dd-mm-yyyy'), '4420-584');

INSERT INTO medicos VALUES (13, 2, NULL, 'Am�lia Silva',   'S�o Jo�o', TO_DATE('17-05-1968','dd-mm-yyyy'), '4150-706');
INSERT INTO medicos VALUES (15, 4, NULL, 'Marcos Marcelo', 'S�o Jo�o', TO_DATE('28-05-1961','dd-mm-yyyy'), '4350-043');
INSERT INTO medicos VALUES (16, 3, NULL, 'Nicolau Vieira', 'S�o Jo�o', TO_DATE('11-09-1968','dd-mm-yyyy'), '4600-012');
INSERT INTO medicos  VALUES (26, 2, 13, 'Angelo Rodrigo',  'S�o Jo�o', TO_DATE('02-02-1973','dd-mm-yyyy'), '4420-282');

-- m?dicos n?o-chefe
INSERT INTO medicos  VALUES (17, 2, 1, 'Ana Moura',       'Pedro Hispano', TO_DATE('13-08-1985','dd-mm-yyyy'), '4454-510');
INSERT INTO medicos  VALUES (18, 1, 2, 'Artur Rocha',     'Pedro Hispano', TO_DATE('25-01-1978','dd-mm-yyyy'), '4490-567');
INSERT INTO medicos  VALUES (19, 4, 3, 'Cl�udia Martins', 'Pedro Hispano', TO_DATE('17-05-1968','dd-mm-yyyy'), '4400-356');

INSERT INTO medicos  VALUES (20, 2, 5, 'Carina Pinto',      'Santa Maria', TO_DATE('27-08-1980','dd-mm-yyyy'), '2750-040');
INSERT INTO medicos  VALUES (21, 4, 7, 'Nelson Vit�ria',    'Santa Maria', TO_DATE('03-09-1961','dd-mm-yyyy'), '2781-902');
INSERT INTO medicos  VALUES (22, 3, 8, 'Patr�cia Carvalho', 'Santa Maria', TO_DATE('12-03-1980','dd-mm-yyyy'), '2720-465');

INSERT INTO medicos  VALUES (25, 3, 12, 'M�rio Nascimento', 'Santo Ant�nio', TO_DATE('13-03-1968','dd-mm-yyyy'), '4440-501');

--consultas
INSERT INTO consultas VALUES (1, TO_TIMESTAMP('25-09-2008 10:10', 'dd-mm-yyyy hh24:mi'), 2, 1);
INSERT INTO consultas VALUES (2, TO_TIMESTAMP('03-09-2011 14:30', 'dd-mm-yyyy hh24:mi'), 2, 1);
INSERT INTO consultas VALUES (3, TO_TIMESTAMP('12-05-2015 15:00', 'dd-mm-yyyy hh24:mi'), 2, 1);
INSERT INTO consultas VALUES (4, TO_TIMESTAMP('23-09-2018 10:30', 'dd-mm-yyyy hh24:mi'), 2, 1);

INSERT INTO consultas VALUES (5, TO_TIMESTAMP('08-03-2015 15:30', 'dd-mm-yyyy hh24:mi'), 17, 2);
INSERT INTO consultas VALUES (6, TO_TIMESTAMP('23-09-2018 15:30', 'dd-mm-yyyy hh24:mi'), 18, 2);
INSERT INTO consultas VALUES (7, TO_TIMESTAMP('28-09-2018 15:30', 'dd-mm-yyyy hh24:mi'), 19, 2);
INSERT INTO consultas VALUES (8, TO_TIMESTAMP('25-09-2018 15:30', 'dd-mm-yyyy hh24:mi'), 4, 2);
INSERT INTO consultas VALUES (9, TO_TIMESTAMP('07-10-2018 10:30', 'dd-mm-yyyy hh24:mi'), 18, 2);

INSERT INTO consultas VALUES (10, TO_TIMESTAMP('15-03-2017 16:30', 'dd-mm-yyyy hh24:mi'), 1, 3);
INSERT INTO consultas VALUES (11, TO_TIMESTAMP('10-10-2017 09:30', 'dd-mm-yyyy hh24:mi'), 17, 3);
INSERT INTO consultas VALUES (12, TO_TIMESTAMP('10-10-2017 12:30', 'dd-mm-yyyy hh24:mi'), 2, 3);
INSERT INTO consultas VALUES (13, TO_TIMESTAMP('10-10-2017 14:00', 'dd-mm-yyyy hh24:mi'), 3, 3);
INSERT INTO consultas VALUES (14, TO_TIMESTAMP('11-10-2017 10:00', 'dd-mm-yyyy hh24:mi'), 4, 3);
INSERT INTO consultas VALUES (15, TO_TIMESTAMP('12-10-2017 16:30', 'dd-mm-yyyy hh24:mi'), 19, 3);
INSERT INTO consultas VALUES (16, TO_TIMESTAMP('07-10-2018 11:30', 'dd-mm-yyyy hh24:mi'), 18, 3);
INSERT INTO consultas VALUES (17, TO_TIMESTAMP('07-10-2018 14:00', 'dd-mm-yyyy hh24:mi'), 19, 3);

INSERT INTO consultas VALUES (18, TO_TIMESTAMP('05-05-2017 16:00', 'dd-mm-yyyy hh24:mi'), 1, 4);
INSERT INTO consultas VALUES (19, TO_TIMESTAMP('09-10-2017 10:30', 'dd-mm-yyyy hh24:mi'), 1, 4);
INSERT INTO consultas VALUES (20, TO_TIMESTAMP('10-10-2017 14:30', 'dd-mm-yyyy hh24:mi'), 2, 4);
INSERT INTO consultas VALUES (21, TO_TIMESTAMP('10-10-2017 15:00', 'dd-mm-yyyy hh24:mi'), 3, 4);
INSERT INTO consultas VALUES (22, TO_TIMESTAMP('11-10-2017 11:00', 'dd-mm-yyyy hh24:mi'), 4, 4);
INSERT INTO consultas VALUES (23, TO_TIMESTAMP('13-10-2017 15:30', 'dd-mm-yyyy hh24:mi'), 19, 4);
INSERT INTO consultas VALUES (24, TO_TIMESTAMP('08-10-2018 11:30', 'dd-mm-yyyy hh24:mi'), 18, 4);

INSERT INTO consultas VALUES (25, TO_TIMESTAMP('15-03-2013 16:30', 'dd-mm-yyyy hh24:mi'), 5, 5);
INSERT INTO consultas VALUES (26, TO_TIMESTAMP('10-10-2017 09:30', 'dd-mm-yyyy hh24:mi'), 20, 5);
INSERT INTO consultas VALUES (27, TO_TIMESTAMP('15-09-2018 16:00', 'dd-mm-yyyy hh24:mi'), 5, 5);
INSERT INTO consultas VALUES (28, TO_TIMESTAMP('05-10-2018 09:00', 'dd-mm-yyyy hh24:mi'), 20, 5);
INSERT INTO consultas VALUES (29, TO_TIMESTAMP('10-10-2017 12:30', 'dd-mm-yyyy hh24:mi'), 6, 5);
INSERT INTO consultas VALUES (30, TO_TIMESTAMP('07-10-2018 11:30', 'dd-mm-yyyy hh24:mi'), 6, 5);
INSERT INTO consultas VALUES (31, TO_TIMESTAMP('01-09-2018 14:00', 'dd-mm-yyyy hh24:mi'), 7, 5);
INSERT INTO consultas VALUES (32, TO_TIMESTAMP('08-10-2017 16:30', 'dd-mm-yyyy hh24:mi'), 21, 5);
INSERT INTO consultas VALUES (34, TO_TIMESTAMP('08-08-2018 12:00', 'dd-mm-yyyy hh24:mi'), 21, 5);
INSERT INTO consultas VALUES (33, TO_TIMESTAMP('11-10-2017 10:00', 'dd-mm-yyyy hh24:mi'), 8, 5);
INSERT INTO consultas VALUES (35, TO_TIMESTAMP('05-10-2018 14:00', 'dd-mm-yyyy hh24:mi'), 22, 5);

INSERT INTO consultas VALUES (37, TO_TIMESTAMP('11-11-2009 14:30', 'dd-mm-yyyy hh24:mi'), 13, 6);
INSERT INTO consultas VALUES (38, TO_TIMESTAMP('22-08-2014 09:30', 'dd-mm-yyyy hh24:mi'), 15, 6);
INSERT INTO consultas VALUES (39, TO_TIMESTAMP('15-04-2015 14:00', 'dd-mm-yyyy hh24:mi'), 13, 6);

-- medicamentos
INSERT INTO medicamentos VALUES (1, 'Aspirina',  'Bayer');
INSERT INTO medicamentos VALUES (2, 'Voltaren',  'Novartis');
INSERT INTO medicamentos VALUES (3, 'Aspegic',   'Infarmed');
INSERT INTO medicamentos VALUES (4, 'Kompensan', 'Infarmed');
INSERT INTO medicamentos VALUES (5, 'Benuron',   'Infarmed');
INSERT INTO medicamentos VALUES (6, 'Xanax',     'Pfizer');
INSERT INTO medicamentos VALUES (7, 'Risidon',   'Infarmed');

---- medicamentos receitados
INSERT INTO medicamentos_receitados VALUES (1, 1); 
INSERT INTO medicamentos_receitados VALUES (1, 3); 
INSERT INTO medicamentos_receitados VALUES (1, 2); 
INSERT INTO medicamentos_receitados VALUES (1, 4); 

INSERT INTO medicamentos_receitados VALUES (2, 1);
INSERT INTO medicamentos_receitados VALUES (2, 4);
INSERT INTO medicamentos_receitados VALUES (2, 5); 
INSERT INTO medicamentos_receitados VALUES (2, 3);

INSERT INTO medicamentos_receitados VALUES (3, 1);
INSERT INTO medicamentos_receitados VALUES (3, 3);
INSERT INTO medicamentos_receitados VALUES (3, 5);
INSERT INTO medicamentos_receitados VALUES (3, 4);

INSERT INTO medicamentos_receitados VALUES (4, 4);
INSERT INTO medicamentos_receitados VALUES (4, 5);
INSERT INTO medicamentos_receitados VALUES (4, 7);
INSERT INTO medicamentos_receitados VALUES (4, 6);

INSERT INTO medicamentos_receitados VALUES (5, 1);
INSERT INTO medicamentos_receitados VALUES (5, 2);

INSERT INTO medicamentos_receitados VALUES (6, 1);
INSERT INTO medicamentos_receitados VALUES (9, 2);

INSERT INTO medicamentos_receitados VALUES (7, 2);
INSERT INTO medicamentos_receitados VALUES (7, 3);

INSERT INTO medicamentos_receitados VALUES (10, 4);

INSERT INTO medicamentos_receitados VALUES (12, 1);
INSERT INTO medicamentos_receitados VALUES (12, 2);
INSERT INTO medicamentos_receitados VALUES (12, 3);
INSERT INTO medicamentos_receitados VALUES (12, 4);

INSERT INTO medicamentos_receitados VALUES (16, 3);

INSERT INTO medicamentos_receitados VALUES (20, 1);
INSERT INTO medicamentos_receitados VALUES (20, 2);
INSERT INTO medicamentos_receitados VALUES (20, 3);
INSERT INTO medicamentos_receitados VALUES (20, 4);

INSERT INTO medicamentos_receitados VALUES (21, 1);
INSERT INTO medicamentos_receitados VALUES (21, 2);
INSERT INTO medicamentos_receitados VALUES (21, 3);
INSERT INTO medicamentos_receitados VALUES (21, 4);

INSERT INTO medicamentos_receitados VALUES (22, 3);
INSERT INTO medicamentos_receitados VALUES (22, 4);

INSERT INTO medicamentos_receitados VALUES (24, 2);

INSERT INTO medicamentos_receitados VALUES (25, 1);
INSERT INTO medicamentos_receitados VALUES (25, 2);
INSERT INTO medicamentos_receitados VALUES (25, 3);
INSERT INTO medicamentos_receitados VALUES (25, 4);

INSERT INTO medicamentos_receitados VALUES (29, 1);
INSERT INTO medicamentos_receitados VALUES (29, 2);

--se necess?rio por causa de problemas com o  REGEXP_LIKE
--ALTER SESSION SET NLS_SORT=BINARY;


--Exerc�cio 1--
--Mostrar o nome dos m�dicos com consultas antes de 04/05/2010.--
--O resultado deve ser apresentado por ordem alfab�tica do nome dos m�dicos.--

select m.nome
from medicos m inner join consultas c
on m.id_medico = c.id_medico
where c.data_hora < TO_DATE('04-05-2010 16:00')
order by 1;


--Exerc�cio 2--
-- Mostrar o nome, a designa��o da especialidade e a localidade dos m�dicos do Hospital de S�o Jo�o.--
--O resultado deve ser apresentado por ordem alfab�tica do nome dos m�dicos.--

select m.nome, e.designacao, cp.localidade
from medicos m inner join especialidades e
on m.id_especialidade = e.id_especialidade
inner join codigos_postais cp
on m.cod_postal = cp.cod_postal
where upper(m.hospital) like 'S�O JO�O'
order by 1;


--Exerc�cio 3--
--Mostrar o nome dos m�dicos cujo n�mero total de medicamentos receitados � superior a 5.--
--O resultado deve ser apresentado por ordem alfab�tica do nome dos m�dicos.--

select m.nome
from medicos m inner join consultas c
on m.id_medico = c.id_medico
inner join medicamentos_receitados mr
on c.id_consulta = mr.id_consulta
group by m.nome
having count(*) > 5;


--Exerc�cio 4--
--Mostrar o nome dos medicamentos mais receitados, ordenados por ordem alfab�tica.--

select md.nome
from medicamentos md inner join medicamentos_receitados mr
on md.id_medicamento = mr.id_medicamento
group by md.nome
having count(*) = (select max(count(*))
                   from medicamentos_receitados mr
                   group by mr.id_medicamento)
order by 1;

--Exerc�cio 4 alternativa--
select m.nome
from medicamentos m
inner join medicamentos_receitados mr
on m.id_medicamento = mr.id_medicamento
group by m.nome
having count(*) = (select max(qtd_medicamento)
                   from((select id_medicamento,count (*) as qtd_medicamento
                   from medicamentos_receitados
                   group by id_medicamento)))
                   order by m.nome;


--Exerc�cio 5--
--Mostrar o nome e o hospital dos m�dicos de Pediatria que consultaram pacientes cuja localidade � Matosinhos.--
--O resultado deve ser apresentado por ordem alfab�tica do nome dos m�dicos.--

select distinct m.nome, m.hospital
from medicos m inner join especialidades e
on m.id_especialidade = e.id_especialidade
inner join consultas c
on m.id_medico = c.id_medico
where upper(e.designacao) like 'PEDIATRIA'
and c.id_consulta in (select c.id_consulta
                      from consultas c inner join pacientes p
                      on c.id_paciente = p.id_paciente
                      inner join codigos_postais cp
                      on p.cod_postal = cp.cod_postal
                      where upper(cp.localidade) like 'MATOSINHOS');

--Exerc�cio 5 alternativa--
select distinct M.nome, M.hospital
from medicos M inner join consultas C on M.id_medico=C.id_medico
where (id_especialidade = (select E.id_especialidade
                           from especialidades E
                           where E.designacao like 'Pediatria')
                           and C.id_paciente = any (select P.id_paciente
                           from pacientes P
                           where P.cod_postal = any ( select CP.cod_postal
                                                    from codigos_postais CP
                                                    where CP.localidade = 'Matosinhos')))
order by 1;


--Exerc�cio 6--
--Mostrar a designa��o das especialidades cujos m�dicos nunca deram consultas ao paciente Alfredo Gama.--
--O resultado deve apresentado por ordem alfab�tica da designa��o da especialidade.--

select distinct e.designacao
from especialidades e inner join medicos m
on e.id_especialidade = m.id_especialidade
where e.id_especialidade not in (select e.id_especialidade
                                 from especialidades e inner join medicos m
                                 on m.id_especialidade = e.id_especialidade
                                 inner join consultas c
                                 on c.id_medico = m.id_medico
                                 inner join pacientes p
                                 on c.id_paciente = p.id_paciente
                                 where upper(p.nome) like 'ALFREDO GAMA')
order by 1;

--Exerc�cio 6 alternativa--
SELECT DISTINCT esp.designacao
FROM especialidades esp
WHERE esp.id_especialidade NOT IN (SELECT med.id_especialidade
                                   FROM pacientes p
                                   INNER JOIN consultas c ON c.id_paciente = p.id_paciente
                                   INNER JOIN medicos med ON med.id_medico = c.id_medico
                                   WHERE p.nome LIKE 'Alfredo Gama');


--Exerc�cio 7--
--Mostrar o nome dos m�dicos que receitaram mais de 3 medicamentos em cada uma das suas consultas.--
--O resultado deve ser apresentado por ordem alfab�tica do nome dos m�dicos.--

select distinct m.nome
from medicos m inner join consultas c
on m.id_medico = c.id_medico
inner join medicamentos_receitados mr
on c.id_consulta = mr.id_consulta
group by m.nome, mr.id_medicamento
having count(mr.id_medicamento) > 3;

--Exerc�cio 7 alternativa--
SELECT DISTINCT m.nome
FROM medicos m
INNER JOIN consultas c ON m.id_medico = c.id_medico
INNER JOIN medicamentos_receitados mr ON c.id_consulta = mr.id_consulta
WHERE NOT EXISTS (SELECT c3.id_consulta
                  FROM consultas c3
                  INNER JOIN medicos m3 ON c3.id_medico = m3.id_medico
                  WHERE m3.id_medico = m.id_medico
                  MINUS
                  SELECT mr2.id_consulta
                  FROM medicamentos_receitados mr2
                  INNER JOIN consultas c2 ON mr2.id_consulta = c2.id_consulta
                  INNER JOIN medicos m2 ON c2.id_medico = m2.id_medico
                  WHERE m2.id_medico = m.id_medico)
GROUP BY m.nome, mr.id_consulta
HAVING COUNT(*) > 3
ORDER BY m.nome;


--Exerc�cio 8--
--Mostrar o nome e o hospital dos m�dicos de Cardiologia que n�o realizaram consultas entre 1 de fevereiro e 31 de maio de 2015.--
--O resultado deve ser apresentado por ordem alfab�tica do nome dos m�dicos.--

select distinct m.nome, m.hospital
from medicos m inner join especialidades e
on m.id_especialidade = e.id_especialidade
where e.designacao like 'Cardiologia' 
and m.id_medico not in (select c.id_medico
                        from consultas c
                        where c.data_hora between TO_DATE('01-02-2015', 'dd-mm-yyyy') and TO_DATE('31-05-2015', 'dd-mm-yyyy'))
order by 1;


--Exerc�cio 9--
--Mostrar o nome dos pacientes que s� foram consultados em Pediatria.--
--O resultado deve ser apresentado por ordem alfab�tica do nome dos pacientes.--

select distinct p.nome
from pacientes p inner join consultas c on p.id_paciente = c.id_paciente
inner join medicos m on c.id_medico = m.id_medico
where p.id_paciente not in (select p.id_paciente
                            from pacientes p inner join consultas c
                            on p.id_paciente = c.id_paciente
                            inner join medicos m
                            on c.id_medico = m.id_medico
                            inner join especialidades e
                            on m.id_especialidade = e.id_especialidade
                            where e.designacao not like 'Pediatria')
order by 1;

--Exerc�cio 9 alternativo--
SELECT DISTINCT p.nome
FROM pacientes p
inner JOIN consultas c ON c.id_paciente = p.id_paciente
MINUS
SELECT DISTINCT p.nome
FROM pacientes p
inner JOIN consultas c ON c.id_paciente = p.id_paciente
INNER JOIN medicos med ON med.id_medico = c.id_medico
INNER JOIN especialidades esp ON esp.id_especialidade = med.id_especialidade
WHERE esp.designacao NOT LIKE 'Pediatria';

--Exerc�cio 9 alternativo2--
SELECT p.nome
FROM pacientes p
INNER JOIN consultas c ON p.id_paciente = c.id_paciente
INNER JOIN medicos m ON c.id_medico = m.id_medico
INNER JOIN especialidades e ON m.id_especialidade = e.id_especialidade
WHERE LOWER(e.designacao) LIKE 'pediatria' 
MINUS 
SELECT p2.nome
FROM pacientes p2
INNER JOIN consultas c2 ON p2.id_paciente = c2.id_paciente
INNER JOIN medicos m2 ON c2.id_medico = m2.id_medico
INNER JOIN especialidades e2 ON m2.id_especialidade = e2.id_especialidade
WHERE e2.designacao NOT LIKE 'Pediatria'


--Exerc�cio 10--
--Mostrar o nome dos pacientes e o hospital em que os pacientes foram consultados por todos os m�dicos desse hospital.--
--O resultado deve ser apresentado por ordem alfab�tica do nome dos pacientes.--

SELECT p.nome, med.hospital, count(DISTINCT med.id_medico) Numero_Medicos
FROM pacientes p
INNER JOIN consultas c ON c.id_paciente = p.id_paciente
INNER JOIN medicos med ON med.id_medico = c.id_medico
GROUP BY p.nome, med.hospital
HAVING count(DISTINCT med.id_medico) = ALL (SELECT count(DISTINCT med1.id_medico)
                                            FROM medicos med1
                                            WHERE med1.hospital = med.hospital 
                                            GROUP BY med1.hospital) AND count(DISTINCT med.id_medico) = (SELECT count(DISTINCT med2.id_medico)
                                                                                                         FROM medicos med2
                                                                                                         WHERE med.hospital = med2.hospital
                                                                                                         GROUP BY med2.hospital)
ORDER BY 1;                                                                                                         
                                                   
--Exerc�cio 10 alternativa--
SELECT p.nome, med.hospital
FROM pacientes p
INNER JOIN consultas c ON c.id_paciente = p.id_paciente
INNER JOIN medicos med ON med.id_medico = c.id_medico
GROUP BY p.nome, med.hospital
HAVING count(DISTINCT med.nome) = ALL (SELECT count(DISTINCT med1.nome)
                                       FROM medicos med1
                                       WHERE med1.hospital = med.hospital 
                                       GROUP BY med1.hospital) AND count(DISTINCT med.nome) = (SELECT count(DISTINCT med2.nome)
                                                                                               FROM medicos med2
                                                                                               WHERE med.hospital = med2.hospital
                                                                                               GROUP BY med2.hospital)
ORDER BY p.nome;

--Exerc�cio 10 alternativa2--
SELECT DISTINCT p.nome, m.hospital
FROM pacientes p 
INNER JOIN consultas c ON p.id_paciente = c.id_paciente
INNER JOIN medicos m ON c.id_medico = m.id_medico
WHERE NOT EXISTS (SELECT m2.id_medico
                  FROM medicos m2
                  WHERE m2.hospital = m.hospital
                  MINUS
                  SELECT c2.id_medico
                  FROM consultas c2
                  INNER JOIN medicos m3 ON c2.id_medico = m3.id_medico
                  WHERE m3.hospital = m.hospital AND c2.id_paciente = c.id_paciente)
ORDER BY p.nome;


--Exerc�cio 11--
--Mostrar o nome dos m�dicos, que receitaram sempre o mesmo n�mero de medicamentos em todas as suas consultas, juntamente com esse n�mero.--
--O resultado deve ser apresentado por ordem alfab�tica do nome dos m�dicos.--

select distinct m.nome, count(mr.id_medicamento)
from medicos m inner join consultas c on m.id_medico = c.id_medico
inner join medicamentos_receitados mr on c.id_consulta = mr.id_consulta
group by m.nome, mr.id_consulta
having count(mr.id_medicamento) = all (select count(mr2.id_medicamento)
                                       from medicamentos_receitados mr2 inner join consultas c2 on mr2.id_consulta = c2.id_consulta
                                       inner join medicos m2 on c2.id_medico = m2.id_medico
                                       where m2.nome = m.nome
                                       group by c2.id_consulta)
                                  AND m.nome NOT IN (select distinct m2.nome
                                                     from medicos m2 inner join consultas c on m2.id_medico = c.id_medico
                                  where c.id_consulta not in (select mr.id_consulta
                                  from medicamentos_receitados mr))
order by 1;

--Exerc�cio 11 alternativa--
select distinct m.nome, count(mr.id_medicamento) as NR_MEDICAMENTOS_RECEITADOS
from medicos m inner join consultas c on c.id_medico = m.id_medico
inner join medicamentos_receitados mr on mr.id_consulta=c.id_consulta
group by m.nome, c.id_consulta
having count(mr.id_medicamento) = All (select count(mr.id_medicamento)
                       from consultas c
                       left outer join medicamentos_receitados mr on c.id_consulta = mr.id_consulta
                       inner join medicos m2 on m2.id_medico=c.id_medico
                       where m2.nome=m.nome 
                       group by c.id_consulta) 
order by 1;


--Exerc�cio 12--
--Mostrar o id e a designa��o das especialidades, juntamente com as respetivas datas em que tiveram o maior n�mero de consultas.--
--O resultado deve ser apresentado por ordem alfab�tica da designa��o da especialidade e por ordem ascendente da data.--
--O comando deve usar a cl�usula WITH que permite a reutiliza��o de c�digo..--

--Exerc�cio 12 com with--
with
consultas_especialidades as 
(
select e.id_especialidade, e.designacao, trunc(c.data_hora) data, count(*) cnt
from consultas c, medicos m, especialidades e
where c.id_medico = m.id_medico and e.id_especialidade = m.id_especialidade
group by e.id_especialidade, e.designacao, trunc(c.data_hora)
)
select id_especialidade, designacao, data
from consultas_especialidades ce1
where cnt = (select max(cnt)
             from consultas_especialidades ce2
             where ce1.id_especialidade = ce2.id_especialidade)
order by 2,3;

--Exerc�cio 12 sem with vers�o 1--
select e.id_especialidade, e.designacao, trunc(c.data_hora)
from especialidades e
join medicos m on e.id_especialidade = m.id_especialidade
join consultas c on m.id_medico = c.id_medico
group by e.id_especialidade, e.designacao, trunc(c.data_hora)
having count(*) =   (select max(count(*))
                    from consultas c2
                    join medicos m on m.id_medico = c2.id_medico
                    join especialidades e2 on e2.id_especialidade = m.id_especialidade
                    where e.id_especialidade=e2.id_especialidade
                    group by trunc(c2.data_hora), e2.id_especialidade )
order by e.designacao, trunc(c.data_hora);

--Exerc�cio 12 sem with vers�o 2--
SELECT esp.id_especialidade, esp.designacao, TRUNC(c.data_hora), count(esp.id_especialidade)
FROM especialidades esp
INNER JOIN medicos med ON med.id_especialidade = esp.id_especialidade
INNER JOIN consultas c ON c.id_medico = med.id_medico
GROUP BY esp.id_especialidade, esp.designacao, TRUNC(c.data_hora)
HAVING count(esp.id_especialidade) = (SELECT MAX(count(esp2.id_especialidade))
                                      FROM especialidades esp2
                                      INNER JOIN medicos med ON med.id_especialidade = esp2.id_especialidade
                                      INNER JOIN consultas c ON c.id_medico = med.id_medico
                                      WHERE esp.id_especialidade = esp2.id_especialidade
                                      GROUP BY esp2.id_especialidade, esp2.designacao, TRUNC(c.data_hora))
ORDER BY esp.designacao, TRUNC(c.data_hora);


--Exerc�cio 13--
--Mostrar o nome dos pacientes que foram consultados por todos os m�dicos do Hospital Pedro Hispano.--

select p.nome 
from pacientes p inner join consultas c on p.id_paciente = c.id_paciente
inner join medicos m on c.id_medico = m.id_medico
where m.hospital like 'Pedro Hispano'
group by p.nome, m.hospital
having count(distinct m.id_medico) = all (select count(m2.id_medico)
                                   from medicos m2
                                   where m2.hospital like 'Pedro Hispano');


--Exerc�cio 13a--
--Mostrar o nome dos pacientes que nunca foram consultados por todos os m�dicos do Hospital Pedro Hispano.--

select distinct p.nome 
from pacientes p inner join consultas c on p.id_paciente = c.id_paciente
inner join medicos m on c.id_medico = m.id_medico
where m.hospital like 'Pedro Hispano'
minus
select p.nome 
from pacientes p inner join consultas c on p.id_paciente = c.id_paciente
inner join medicos m on c.id_medico = m.id_medico
where m.hospital like 'Pedro Hispano'
group by p.nome, m.hospital
having count(distinct m.id_medico) = all (select count(m2.id_medico)
                                   from medicos m2
                                   where m2.hospital like 'Pedro Hispano')
order by 1;


--Exerc�cio 14--
--Mostrar o nome, o hospital e a data de nascimento do m�dico mais novo de cada hospital.--
--O resultado deve ser apresentado por ordem ascendente da data de nascimento do m�dico.--

select distinct m.nome, m.data_nascimento, m.hospital
from medicos m
where m.data_nascimento = (select min(m2.data_nascimento)
                           from medicos m2
                           where m2.hospital = m.hospital)                          
order by 2;


