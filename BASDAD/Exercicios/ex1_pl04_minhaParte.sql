create table aluno(
numero char(7),
nome varchar(25),
telefone varchar(20)
);

create table disciplina(
codigo varchar(4),
nome varchar(50)
);

create table inscricao(
aluno char(7),
disciplina varchar(4),
ano number(4),
semestre number(1),
aprovado char(1),
classif number(2)
);

alter table aluno add morada char(50);
alter table aluno modify morada varchar(50);