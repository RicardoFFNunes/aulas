DROP TABLE medicamentos;
DROP TABLE medicamentos_receitados;
DROP TABLE consultas;
DROP TABLE pacientes;
DROP TABLE codigos_postais;
DROP TABLE especialidades;
DROP TABLE medicos;

CREATE TABLE medicamentos(
id_medicamento INTEGER CONSTRAINT pk_medicamentos_id_medicamento PRIMARY KEY,
nome VARCHAR(30) NOT NULL,
laboratorio CHAR(15),
preco NUMERIC(5,2),
CONSTRAINT ck_medicamentos_laboratorio CHECK (laboratorio LIKE '__-%'),
CONSTRAINT ck_medicamentos_preco CHECK (preco >= 30)
);

CREATE TABLE medicamentos_receitados(
id_medicamento,
id_consulta,
CONSTRAINT fk_medicamentos_receitados_id_medicamento FOREIGN KEY (id_medicamento) REFERENCES medicamentos(id_medicamento),
CONSTRAINT fk_medicamentos_receitados_id_consulta FOREIGN KEY (id_consulta) REFERENCES consultas(id_consulta)
);

CREATE TABLE consultas(
id_consulta INTEGER CONSTRAINT pk_consultas_id_consulta PRIMARY KEY,
id_medico INTEGER,
data_hora DATE,
CONSTRAINT fk_medicos_id_medico FOREIGN KEY(id_medico) REFERENCES medicos(id_medico)
);

CREATE TABLE medicos(
id_medico INTEGER CONSTRAINT pk_medicos_id_medico PRIMARY KEY,
id_especialidade INTEGER,
id_medico_chefe INTEGER,
cod_postal CHAR(8),
nome VARCHAR(30),
hospital VARCHAR(30),
data_nascimento DATE,
CONSTRAINT fk_medicos_id_especialidade FOREIGN KEY (id_especialidade) REFERENCES especialidades(especialidade_ID),
CONSTRAINT fk_medicos_id_medico_chefe FOREIGN KEY (id_medico_chefe) REFERENCES medicos(id_medico),
CONSTRAINT fk_medicos_codigo_postal FOREIGN KEY (cod_postal) REFERENCES codigos_postais(cod_postal)
);

CREATE TABLE pacientes(
id_paciente INTEGER CONSTRAINT pk_pacientes_id_paciente PRIMARY KEY,
cod_postal CHAR(8),
nome VARCHAR(30),
data_nascimento DATE,
CONSTRAINT fk_pacientes_cod_postal FOREIGN KEY (cod_postal) REFERENCES codigos_postais(cod_postal)
);

CREATE TABLE codigos_postais(
cod_postal CHAR(8) CONSTRAINT pk_codigos_postais_cod_postal PRIMARY KEY,
localidade VARCHAR(30),
CONSTRAINT ck_codigos_postais_cod_postal CHECK (cod_postal LIKE '____-___')
);

CREATE TABLE especialidades(
especialidade_ID INTEGER CONSTRAINT pk_especialidades_id_especialidade PRIMARY KEY,
designacao VARCHAR(30)
);

ALTER TABLE consultas ADD id_paciente INTEGER;
ALTER TABLE consultas ADD CONSTRAINT fk_consultas_id_paciente FOREIGN KEY (id_paciente) REFERENCES pacientes (id_paciente);

insert into especialidades values(1, 'Oncologia');
insert into especialidades values(2, 'Ginecologia');
insert into especialidades values(3, 'Urologia');
insert into especialidades values(4, 'Estomatologia');
insert into especialidades values(5, 'Psiquiatria');

insert into codigos_postais values('4000-123', 'Porto');
insert into codigos_postais values('1000-821', 'Lisboa');
insert into codigos_postais values('6000-552', 'Castelo Branco');
insert into codigos_postais values('9000-516', 'Funchal');
insert into codigos_postais values('9600-566', 'Azores');

insert into medicamentos values(01, 'Benetron', '01-Isep--------', 31.00);
insert into medicamentos values(12, 'Brufen', '12-Pfizer------', 50.50);
insert into medicamentos values(13, 'Aspirina', '13-Bayern------', 110.50);
insert into medicamentos values(14, 'Parasetamol', '14-Mylan-------', 37.25);
insert into medicamentos values(05, 'Valeriana', '05-Tua avo-----', 45.00);

insert into medicamentos values(11, 'Benetron', '11-Isep-Technol', 31.00);
insert into medicamentos values(17, 'Brufen', '17-Pfizer Biotc', 50.50);
insert into medicamentos values(13, 'Aspirina', '13-Bayern Munch', 110.50);
insert into medicamentos values(14, 'Parasetamol', '14-Mylan Bergam', 37.25);
insert into medicamentos values(15, 'Valeriana', '15-Avo Augustin', 45.00);

insert into pacientes values(111, '6000-552','Violeta', to_date('1974/04/04','yyyy/mm/dd'));
insert into pacientes values(112, '6000-552','Vasco', to_date('1999/12/25','yyyy/mm/dd'));
insert into pacientes values(113, '4000-123','Vanessa', to_date('1988/01/08','yyyy/mm/dd'));
insert into pacientes values(114, '4000-123','Viviane', to_date('2002/11/11','yyyy/mm/dd'));
insert into pacientes values(115, '1000-821','Virgilio', to_date('2007/08/17','yyyy/mm/dd'));
insert into pacientes values(116, '9000-516','Vinicius', to_date('1995/10/12','yyyy/mm/dd'));
insert into pacientes values(117, '9600-566','Vonia', to_date('1997/07/02','yyyy/mm/dd'));
insert into pacientes values(118, '9600-566','Valente', to_date('1996/12/01','yyyy/mm/dd'));

insert into medicos values(001, 3, 001, '6000-552', 'Rita Pererira', 'S. Nuno', to_date('1982/03/13','yyyy/mm/dd'));
insert into medicos values(002, 1, 002, '4000-123', 'Pinto da Costa', 'S. Jo�o', to_date('1937/12/28','yyyy/mm/dd'));
insert into medicos values(003, 1, 002, '4000-123', 'Sergio', 'S. Jo�o', to_date('1974/11/15','yyyy/mm/dd'));
insert into medicos values(004, 4, 004, '1000-821', 'Batatinha', 'Todos os Santos', to_date('1962/01/16','yyyy/mm/dd'));
insert into medicos values(005, 4, 004, '1000-821', 'Companhia', 'Todos os Santos', to_date('1961/04/07','yyyy/mm/dd'));
insert into medicos values(006, 2, 006, '9000-516', 'Cristiano', 'S. Jo�o Jardim', to_date('1985/02/05','yyyy/mm/dd'));
insert into medicos values(007, 5, 007, '9600-566', 'Pauleta', 'S. Paleta', to_date('1973/04/28','yyyy/mm/dd'));
insert into medicos values(008, 1, 007, '9600-566', 'Henriques', 'S. Paleta', to_date('1394/03/04','yyyy/mm/dd'));

insert into consultas values(10091, 001, to_date('2020/03/04','yyyy/mm/dd'),111);
insert into consultas values(10092, 002, to_date('2020/04/12','yyyy/mm/dd'),113);
insert into consultas values(10093, 002, to_date('2020/04/13','yyyy/mm/dd'),113);
insert into consultas values(10094, 003, to_date('2020/11/05','yyyy/mm/dd'),114);
insert into consultas values(10095, 004, to_date('2020/01/05','yyyy/mm/dd'),115);
insert into consultas values(10098, 005, to_date('2020/01/05','yyyy/mm/dd'),115);
insert into consultas values(10096, 006, to_date('2020/05/17','yyyy/mm/dd'),116);
insert into consultas values(10097, 008, to_date('2020/03/25','yyyy/mm/dd'),118);

insert into medicamentos_receitados values(11, 10091);
insert into medicamentos_receitados values(17, 10091);
insert into medicamentos_receitados values(15, 10091);
insert into medicamentos_receitados values(15, 10092);
insert into medicamentos_receitados values(15, 10093);
insert into medicamentos_receitados values(13, 10093);
insert into medicamentos_receitados values(14, 10093);
insert into medicamentos_receitados values(11, 10094);
insert into medicamentos_receitados values(13, 10094);
insert into medicamentos_receitados values(15, 10095);
insert into medicamentos_receitados values(13, 10095);
insert into medicamentos_receitados values(17, 10096);
insert into medicamentos_receitados values(14, 10096);
insert into medicamentos_receitados values(15, 10097);
insert into medicamentos_receitados values(11, 10098);
insert into medicamentos_receitados values(13, 10098);

--2.
SELECT m.nome, e.designacao
FROM medicos m INNER JOIN especialidades e ON m.id_especialidade = e.especialidade_id
WHERE LOWER(m.hospital) LIKE 's. jo�o'
ORDER BY 1;

--3.
SELECT m.nome
FROM medicamentos m
INNER JOIN medicamentos_receitados m_r ON m.id_medicamento = m_r.id_medicamento
GROUP BY m.nome
HAVING COUNT(*) = (SELECT MIN(COUNT(*))
                  FROM medicamentos_receitados m_r
                  GROUP BY m_r.id_medicamento); 

--4.           
SELECT DISTINCT e.designacao
FROM especialidades e
INNER JOIN medicos m ON e.especialidade_id = m.id_especialidade
WHERE m.id_medico NOT IN (SELECT m.id_medico
                          FROM medicos m
                          INNER JOIN consultas c ON c.id_medico = m.id_medico
                          INNER JOIN pacientes p ON c.id_paciente = p.id_paciente
                          WHERE LOWER(p.nome) LIKE 'luciana');

--Inserir dados novos para ser mais evidente o resultado:
INSERT INTO especialidades VALUES (6,'Medicina Interna');
INSERT INTO medicos VALUES(009,6,008,'1000-821','Quaresma','Todos os Santos',TO_DATE('1955/11/13','yyyy/mm/dd'));
INSERT INTO pacientes VALUES(119,'1000-821','Luciana', to_date('1992/08/28','yyyy/mm/dd'));
INSERT INTO consultas VALUES(123,009,to_date('2020/03/04','yyyy/mm/dd'),119);

--�nico m�dico que atendeu a Luciana � o 9 (Quaresma), que por sua vez � o �nico da sua especialidade (Medicina Interna).
--Logo, o resultado vai mostrar todas as especialidades menos esta.
SELECT m.id_medico
FROM medicos m
INNER JOIN consultas c ON c.id_medico = m.id_medico
INNER JOIN pacientes p ON c.id_paciente = p.id_paciente
WHERE LOWER(p.nome) LIKE 'luciana';

--5.

--Listagem do n�mero de medicamentos por consulta:
SELECT id_consulta, COUNT(*)
FROM medicamentos_receitados mr
GROUP BY id_consulta
ORDER BY id_consulta

--"sempre o mesmo n�mero de medicamentos": n�mero de medicamentos para determinado m�dico numa consulta � igual a todos os n�s de medicamentos noutras consultas.
--Operador = ALL: igual a todos os resultados de uma query

--N� medicamentos receitados por cada m�dico em cada consulta:
--S� o Pinto da Costa � que receitou n� diferente de medicamentos
SELECT m.nome, mr.id_consulta, COUNT(*)
FROM medicos m
INNER JOIN consultas c ON m.id_medico = c.id_medico
INNER JOIN medicamentos_receitados mr ON c.id_consulta = mr.id_consulta
GROUP BY m.nome, mr.id_consulta;

--S� o Pinto da Costa � que receitou n� diferente de medicamentos, logo � o n�o aparece nesta query
--O COUNT(*) na query exterior � o n� de medicamentos receitados por esse m�dico (igual para todas as suas consultas)
SELECT m.nome, COUNT(*)
FROM medicos m
INNER JOIN consultas c ON m.id_medico = c.id_medico
INNER JOIN medicamentos_receitados mr ON c.id_consulta = mr.id_consulta
GROUP BY m.nome, mr.id_consulta --Agrupar por nome e id_consulta de forma a sabe o n� de medicamentos receitados por cada m�dico em cada consulta
HAVING COUNT(*) = ALL (SELECT COUNT(*)
                       FROM consultas c
                       INNER JOIN medicos m2 ON c.id_medico = m2.id_medico
                       INNER JOIN medicamentos_receitados mr ON c.id_consulta = mr.id_consulta
                       WHERE m2.nome = m.nome --necess�rio relacionar com a query exterior
                       GROUP BY mr.id_consulta)
ORDER BY m.nome;


                 
