--eliminar tabelas (eventualmente) existentes

DROP TABLE marinheiros  CASCADE CONSTRAINTS PURGE;
DROP TABLE barcos       CASCADE CONSTRAINTS PURGE;
DROP TABLE reservas     CASCADE CONSTRAINTS PURGE;

--criar tabelas

CREATE TABLE marinheiros(
  id_marinheiro 	INTEGER 	CONSTRAINT pk_marinheiros_id_marinheiro PRIMARY KEY,
  nome 	            VARCHAR(30)	CONSTRAINT nn_marinheiros_nome          NOT NULL,
  classificacao 	INTEGER		CONSTRAINT nn_marinheiros_classificacao NOT NULL,
  idade 		    INTEGER	    CONSTRAINT nn_marinheiros_idade         NOT NULL
);

CREATE TABLE barcos(
  id_barco 	INTEGER     CONSTRAINT pk_barcos_id_barcos 	PRIMARY KEY,
  nome      VARCHAR(20) CONSTRAINT nn_barcos_nome       NOT NULL,
  cor       VARCHAR(10) CONSTRAINT nn_barcos_cor        NOT NULL
);

CREATE TABLE reservas(
  id_marinheiro INTEGER,
  id_barco 	    INTEGER,
  data 	        DATE        CONSTRAINT nn_reservas_data  NOT NULL, 
  CONSTRAINT pk_reservas_id_marinheiro_id_barco PRIMARY KEY(id_marinheiro, id_barco, data)
);

ALTER TABLE reservas ADD CONSTRAINT fk_reservas_id_marinheiro   FOREIGN KEY (id_marinheiro) REFERENCES marinheiros(id_marinheiro);
ALTER TABLE reservas ADD CONSTRAINT fk_reservas_id_barco        FOREIGN KEY (id_barco)      REFERENCES barcos(id_barco);

--preencher tabelas
  
INSERT INTO marinheiros VALUES(22, 'Dustin',  7, 45);
INSERT INTO marinheiros VALUES(29, 'Brutus',  1, 33);
INSERT INTO marinheiros VALUES(31, 'Lubber',  8, 55);
INSERT INTO marinheiros VALUES(32, 'Andy',    8, 25);
INSERT INTO marinheiros VALUES(58, 'Rusty',  10, 35);
INSERT INTO marinheiros VALUES(64, 'Hor�cio', 7, 35);
INSERT INTO marinheiros VALUES(71, 'Zorba',  10, 16);
INSERT INTO marinheiros VALUES(74, 'Hor�cio', 9, 35);
INSERT INTO marinheiros VALUES(85, 'Art',     3, 25);
INSERT INTO marinheiros VALUES(95, 'Bob',     3, 63);
INSERT INTO marinheiros VALUES(13, 'Popeye',  3, 22);
INSERT INTO marinheiros VALUES(44, 'Haddock', 3, 63);

INSERT INTO barcos VALUES(101, 'Interlake', 'azul');
INSERT INTO barcos VALUES(102, 'Interlake', 'vermelho');
INSERT INTO barcos VALUES(103, 'Clipper',   'verde');
INSERT INTO barcos VALUES(104, 'Marine',    'vermelho');
  
INSERT INTO reservas VALUES(22, 101, TO_DATE('2017/10/10','yyyy/mm/dd'));
INSERT INTO reservas VALUES(22, 102, TO_DATE('2017/10/10','yyyy/mm/dd'));
INSERT INTO reservas VALUES(22, 103, TO_DATE('2017/08/10','yyyy/mm/dd'));
INSERT INTO reservas VALUES(22, 104, TO_DATE('2017/07/10','yyyy/mm/dd'));
INSERT INTO reservas VALUES(31, 102, TO_DATE('2017/10/11','yyyy/mm/dd'));
INSERT INTO reservas VALUES(31, 103, TO_DATE('2017/06/11','yyyy/mm/dd'));
INSERT INTO reservas VALUES(31, 104, TO_DATE('2017/12/11','yyyy/mm/dd'));
INSERT INTO reservas VALUES(64, 101, TO_DATE('2017/05/09','yyyy/mm/dd'));
INSERT INTO reservas VALUES(64, 102, TO_DATE('2017/08/09','yyyy/mm/dd'));
INSERT INTO reservas VALUES(64, 102, TO_DATE('2017/09/09','yyyy/mm/dd'));
INSERT INTO reservas VALUES(64, 102, TO_DATE('2017/10/09','yyyy/mm/dd'));
INSERT INTO reservas VALUES(74, 103, TO_DATE('2017/08/09','yyyy/mm/dd'));
INSERT INTO reservas VALUES(44, 101, TO_DATE('2018/05/09','yyyy/mm/dd'));
INSERT INTO reservas VALUES(44, 101, TO_DATE('2018/09/09','yyyy/mm/dd'));

--A.1.
--Queremos saber os marinheiros mais velhos, aqueles cuja idade � igual ao m�ximo de idades de todos marinheiros.
--Subquery n�o correlacionada � corrida apenas uma vez. Basta correr uma vez a subquery para sabermos o m�ximo, ou seja n�o precisamos de calcular um m�ximo para cada marinheiro no SELECT (como nas subqueries correlacionadas).
--Ele vai executar primeiro a subquery para deteminar o m�ximo. Depois faz o filtro no where (� como dissesse depois WHERE idade = 63).
SELECT id_marinheiro, nome,idade
FROM marinheiros
WHERE idade = (SELECT MAX(idade) FROM marinheiros);

--A.2.
--Usar o NOT IN na cl�usula WHERE: significa que vamos filtrar (WHERE) os marinheiros com base na n�o perten�a (NOT IN)de um certo atributo a um conjunto de valores.
--Esse atributo ser� o id_marinheiro, pois queremos saber aqueles que n�o fizeram reservas e o id_marinheiro � o que faz a rela��o entre a tabela marinheiros e a tabela reservas.
--Se um marinheiro fez uma reserva, o seu id estar� na tabela reservas. Se queremos os que n�o fizeram reservas, vemos os id_marinheiro que n�o est�o na tabela reservas, ou seja o id_marinheiro NOT IN (subquery que retorna os id_marinheiro da tabela reservas).
SELECT id_marinheiro, nome
FROM marinheiros
WHERE id_marinheiro NOT IN (SELECT id_marinheiro FROM reservas)
ORDER BY 1;

--A.3.
--Determinar para cada marinheiro a diferen�a entre a sua idade e a m�dia de todas as idades (AVG(idade) FROM marinheiros).
--Fun��o TRUNC para arredondar a diferen�a a 0 casas decimais.
--Ordenar pelo valor absoluto (ABS) de forma decrescente.
SELECT id_marinheiro, nome, TRUNC(((SELECT AVG(idade) FROM marinheiros)-idade),0) AS Diferen�a_para_idade_media
FROM marinheiros 
ORDER BY ABS(Diferen�a_para_idade_media) DESC;

--A.4.
--Queremos os marinheiros que reservaram barcos verdes E barcos vermelhos.
--O INTERSECT permite fazer a intersec��o dos id_marinheiro que aparecem em ambos os "conjuntos" - conjunto marinheiros que reservaram barcos verdes e conjunto de marinheiros que reservaram barcos vermelhos.
--Esta subquery s� � corrida uma vez pois n�o depende da query exterior.
--Depois de ser corrida faz-se apenas uma contagem do n�mero de linhas retornada pela subquery.

SELECT COUNT(*) AS Nr_Total_Marinheiros
FROM (SELECT id_marinheiro
      FROM reservas INNER JOIN barcos ON reservas.id_barco = barcos.id_barco
      WHERE LOWER(barcos.cor) = 'verde'
      INTERSECT 
      SELECT id_marinheiro
      FROM reservas INNER JOIN barcos ON reservas.id_barco = barcos.id_barco
      WHERE LOWER(barcos.cor) = 'vermelho');
      
--Resultado do INTERSECT:
SELECT id_marinheiro
FROM reservas INNER JOIN barcos ON reservas.id_barco = barcos.id_barco
WHERE LOWER(barcos.cor) = 'verde'
INTERSECT 
SELECT id_marinheiro
FROM reservas INNER JOIN barcos ON reservas.id_barco = barcos.id_barco
WHERE LOWER(barcos.cor) = 'vermelho';      
      
--A.5.
--1� passo: se tem cl�usula Having, tem de ter Group By; por isso vou listar todas as datas de reservas e contar o n� de vezes que aparecem na tabela reservas
SELECT data, COUNT(*)
FROM reservas
GROUP BY data; --Group By tem de existir por causa da fun��o de agrega��o COUNT(*)

--2� passo: na cl�usula Having tenho de colocar uma subquery que me permita descobrir quais as datas com maior n� de reservas. Leia-se "Considerando que a contagem � igual ao m�ximo de contagem de todas as datas".

--"M�ximo de contagem de todas as datas":
SELECT MAX(COUNT(data)) 
FROM reservas
GROUP BY data; --fun��o de agrega��o no Select (MAX), logo temos de fazer um GROUP BY

--3� Passo: combinando tudo, fica assim:
SELECT data --no enunciado n�o pede o COUNT(*) logo retirei
FROM reservas
GROUP BY data
HAVING COUNT(*) = (SELECT MAX(COUNT(data)) FROM reservas GROUP BY data);

--A.6.
--C�digo da al�nea A.1.:
SELECT id_marinheiro, nome,idade
FROM marinheiros
WHERE idade = (SELECT MAX(idade) FROM marinheiros);

--Usar o comando ALL na cl�usula WHERE significa: queremos retornar algo cujo campo de compara��o (neste caso, a idade) seja igual, inferior ou superior a TODOS os resultados retornados pela subquery � direita do ALL.
--Os campos de compara��o entre a query exterior e a subquery t�m de ser em igual n�mero e de dom�nios compat�veis.
--Neste caso, queremos saber os marinheiros mais velhos. Assim sendo, queremos os marinheiros cuja idade seja igual a todos os m�ximos de idade da tabela marinheiros.
--N�o resultaria > ALL (SELECT MAX(idade) FROM marinheiros) pois isso iria retornar um conjunto vazio (imposs�vel existirem marinheiros com idade superior ao m�ximo de idades!)

SELECT id_marinheiro, nome,idade
FROM marinheiros
WHERE idade = ALL (SELECT MAX(idade) FROM marinheiros);

--A.7.
--Usar o comando ANY na cl�usula WHERE significa: queremos retornar algo cujo campo de compara��o (neste caso, a idade) seja igual, inferior ou superior a PELO MENOS UM resultado retornado pela subquery � direita do ANY.
--Os campos de compara��o entre a query exterior e a subquery t�m de ser em igual n�mero e de dom�nios compat�veis.
--Se queremos todos os marinheiros menos os mais velhos, basta retornar todos os marinheiros cuja idade � inferior a pelo menos uma das idades de toda a tabela. S� os que t�m a idade m�xima (os mais velhos) � que n�o ser�o retornados, pois a idade desses n�o � inferior a nenhuma das idades da tabela.
SELECT id_marinheiro, nome,idade
FROM marinheiros
WHERE idade < ANY (SELECT idade FROM marinheiros);

--Falta s� ordenar os resultados por ordem decrescente da idade:
SELECT id_marinheiro, nome,idade
FROM marinheiros
WHERE idade < ANY (SELECT idade FROM marinheiros)
ORDER BY idade DESC;

--A.8.
--No enunciado pedem uma SC (subquery correlacionada). Significa que � uma subquery que faz liga��o com um par�metro do select exterior. 
--Ao contr�rio das subqueries n�o correlacionadas, as correlacionadas ser�o executadas n�o apenas uma vez, mas quantas vezes forem chamadas pela subquery exterior.
--Neste caso, a subquery ser� chamada para cada id_marinheiro uma vez que pretendemos saber quantas reservas fez CADA UM dos marinheiros

--1� passo: escrever a estrutura da query exterior (n�o executar, vai dar erro)
SELECT m.id_marinheiro, m.nome, () AS Nr_Total_Reservas
FROM marinheiros
ORDER BY Nr_Total_Reservas DESC; --� pedido que se ordene por ordem decrescente do n� de reservas

--2� passo: escrever a l�gica da subquery - queremos listar o n�mero de reservas (COUNT(*)) de cada marinheiro
SELECT reservas.id_marinheiro, COUNT(*)
FROM reservas, marinheiros
WHERE reservas.id_marinheiro = marinheiros.id_marinheiro --para retornar informa��o sobre as reservas terei de inevitavelmente relacionar a tabela marinheiros com a tabela de reservas atrav�s do seu atributo de liga��o (marinheiro_id)
GROUP BY reservas.id_marinheiro; --uma vez que existe fun��o de agrega��o no SELECT (COUNT(*))

--3� passo: adaptar a query do 2� passo � query do 1� passo
SELECT id_marinheiro, nome,(SELECT COUNT(*)
                            FROM reservas
                            WHERE reservas.id_marinheiro = marinheiros.id_marinheiro --� por aqui que relacionamos a subquery com a query exterior (n�o precisamos de incluir a tabela marinheiros como no passo 2 pois essa j� est� na query exterior)
                            ) Nr_Total_Reservas --n�o faz sentido ter Group By na subquery porque queremos calcular o n� de reservas para cada um dos marinheiros indicados na query exterior, e n�o calcular para todos os marinheiros dentro da subquery
FROM marinheiros
ORDER BY Nr_Total_Reservas DESC;

--B.2.
--1� passo: se vamos inserir uma subquery no HAVING significa que temos um GROUP BY antes, e vamos filtrar os resultados desse GROUP BY com base na subquery.
--Ora, segundo a figura, o agrupamento em quest�o � o n�mero total de reservas por conjunto marinheiro-barco. Por isso, em 1� lugar podemos fazer esse agrupamento.

--Listar n� de reservas de cada barco por marinheiro
SELECT reservas.id_marinheiro, reservas.id_barco, COUNT(*) Nr_Total_Reservas
FROM reservas
GROUP BY reservas.id_marinheiro, reservas.id_barco
ORDER BY reservas.id_marinheiro; --para uma maior clareza

--2� passo: definir a l�gica da subquery. Queremos apenas as combina��es do 1� passo que expressem um n� de reservas superior ao n�mero m�dio de reservas de CADA UM dos barcos.
-- Exemplo: barco 101 foi reservado 1 vez pelo marinheiro 22, 2 vezes pelo marinheiro 44 e 1 vez pelo marinheiro 64. A m�dia de reservas deste barco � 1.(33) ((1+2+1)/3). Consultando a 1� query, apenas o marinheiro 44 reservou o barco 101 num n� de vezes superior � media (2 > 1.(33)).

--Ex., n� de reservas de cada barco:
SELECT reservas.id_barco, COUNT(id_barco)
FROM reservas --tem de ser desta tabela e n�o dos barcos
GROUP BY reservas.id_barco;

--Ex., n� de reservas de um barco espec�fico (101) por cada marinheiro:
SELECT reservas.id_marinheiro, reservas.id_barco,COUNT(*)
FROM reservas --tem de ser desta tabela e n�o dos barcos
WHERE reservas.id_barco = 101
GROUP BY reservas.id_marinheiro , reservas.id_barco;

--Ex., m�dia de reservas de um barco espec�fico (101):
SELECT AVG(COUNT(*))
FROM reservas --tem de ser desta tabela e n�o dos barcos
WHERE reservas.id_barco = 101
GROUP BY reservas.id_marinheiro , reservas.id_barco; --a m�dia � calculada considerando o n� de reservas de um barco por cada marinheiro, da� que tenhamos de fazer a m�dia(AVG) dos COUNT(*) que surgem em cada combina��o marinheiro-barco.

--3�passo: unir a l�gica da query exterior com a subquery.
--Portanto, queremos filtrar os agrupamentos da query exterior com base no COUNT(*) do SELECT, indicando apenas os casos em que o COUNT(*) � superior � media das reservas de um barco.
--Assim sendo, o COUNT(*) � o atributo de correla��o. A compara��o com a m�dia ser� feita para cada COUNT(*), ou seja para cada n� de reservas feita pelo marinheiro X do barco Y.

SELECT reservas.id_marinheiro, reservas.id_barco, COUNT(*) Nr_Total_Reservas
FROM reservas
GROUP BY reservas.id_marinheiro, reservas.id_barco
HAVING COUNT(*)>(SELECT AVG(COUNT(*))
                 FROM reservas
                 GROUP BY reservas.id_marinheiro, reservas.id_barco);

--B.3.
--Objetivo: retornar marinheiros que reservaram TODOS os barcos Interlake
--1� passo: quais s�o os barcos Interlake? (n�o � obrigat�rio saber, s� para ser mais f�cil de visualizar)
SELECT barcos.id_barco, barcos.nome
FROM barcos;
--Resposta: barcos 101 e 102. Ou seja, queremos os marinheiros que reservaram AMBOS os barcos 101 e 102 (n�o apenas o 101 ou apenas o 102).

--Utilizar o NOT EXISTS: queremos os marinheiros que retornem false na subquery, isto � que n�o apare�am como resultado da subquery.
--A subquery ter� ent�o que retornar os marinheiros que n�o tenham reservado barcos Interlake ou que tenham reservado apenas um deles.

--Dividindo o problema em v�rias fases, desta forma ajudando a perceber o funcionamento do EXISTS:
--Exemplo: quais os marinheiros que fizeram reservas?                  
SELECT marinheiros.id_marinheiro, marinheiros.nome
FROM marinheiros
WHERE EXISTS (SELECT *
              FROM reservas
              WHERE reservas.id_marinheiro = marinheiros.id_marinheiro);
                  
--Exemplo: quais os marinheiros que n�o fizeram reservas?                  
SELECT marinheiros.id_marinheiro, marinheiros.nome
FROM marinheiros
WHERE NOT EXISTS (SELECT *
                  FROM reservas
                  WHERE reservas.id_marinheiro = marinheiros.id_marinheiro);

--Exemplo: quais os marinheiros que fizeram reservas de barcos Interlake 101?                 
SELECT marinheiros.id_marinheiro, marinheiros.nome
FROM marinheiros
WHERE EXISTS (SELECT *
              FROM reservas, barcos
              WHERE reservas.id_marinheiro = marinheiros.id_marinheiro
              AND reservas.id_barco = barcos.id_barco
              AND reservas.id_barco = 101);
              
--Exemplo: quais os marinheiros que fizeram reservas de barcos Interlake 102?                 
SELECT marinheiros.id_marinheiro, marinheiros.nome
FROM marinheiros
WHERE EXISTS (SELECT *
              FROM reservas, barcos
              WHERE reservas.id_marinheiro = marinheiros.id_marinheiro
              AND reservas.id_barco = barcos.id_barco
              AND reservas.id_barco = 102);
              
--Exemplo: quais os marinheiros que fizeram reservas de barcos Interlake 102 OU Interlake 101?                 
SELECT marinheiros.nome
FROM marinheiros
WHERE EXISTS (SELECT *
              FROM reservas, barcos
              WHERE reservas.id_marinheiro = marinheiros.id_marinheiro
              AND reservas.id_barco = barcos.id_barco
              AND barcos.id_barco IN (101,102));
              
--Exemplo: quais os marinheiros que fizeram reservas de barcos Interlake?                 
SELECT marinheiros.nome
FROM marinheiros
WHERE EXISTS (SELECT *
              FROM reservas, barcos
              WHERE reservas.id_marinheiro = marinheiros.id_marinheiro
              AND reservas.id_barco = barcos.id_barco
              AND barcos.nome LIKE 'Interlake');
              
--Exemplo: quais os marinheiros que N�O fizeram reservas de barcos Interlake 102 ou Interlake 101?
--Nota: o Hor�cio que resulta desta query � o de id 74, n�o o de id 64 que � o que reservou barcos 101 ou 102
SELECT marinheiros.nome
FROM marinheiros
WHERE NOT EXISTS (SELECT *
              FROM reservas, barcos
              WHERE reservas.id_marinheiro = marinheiros.id_marinheiro
              AND reservas.id_barco = barcos.id_barco
              AND barcos.id_barco IN (101,102));
                  
--Confirma��o:
SELECT reservas.id_marinheiro, marinheiros.nome, reservas.id_barco, reservas.data
FROM reservas INNER JOIN marinheiros ON reservas.id_marinheiro = marinheiros.id_marinheiro
ORDER BY reservas.id_marinheiro, reservas.id_barco;

--**THIS IS NOT WORKING**--
SELECT marinheiros.nome
FROM marinheiros
WHERE NOT EXISTS (SELECT *
                 FROM reservas, barcos
                 WHERE reservas.id_marinheiro = marinheiros.id_marinheiro --relacionar com a query exterior
                 AND reservas.id_barco = barcos.id_barco --relacionar com a tabela barcos para conseguir filtrar pelo nome do barco
                 AND barcos.nome NOT LIKE 'Interlake');
