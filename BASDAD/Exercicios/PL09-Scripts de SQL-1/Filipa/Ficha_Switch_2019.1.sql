--eliminar tabelas (eventualmente) existentes

drop table especialidades  cascade constraints purge;
drop table medicamentos       cascade constraints purge;
drop table consultas     cascade constraints purge;
drop table medicos     cascade constraints purge;
drop table medicamentos_receitados     cascade constraints purge;
drop table pacientes     cascade constraints purge;
drop table codigos_postais     cascade constraints purge;

--criar tabelas

create table especialidades (
    id_especialidade numeric primary key,
    designacao varchar(25)
);


create table codigos_postais(
    cod_postal numeric primary key,
    localidade varchar(25)
);

create table medicamentos(
    id_medicamento numeric primary key,
    nome varchar(25),
    laboratorio varchar(25),
    preco numeric
);

create table medicos(
    id_medico numeric primary key,
    id_especialidade numeric,
    id_medico_chefe numeric,
    cod_postal numeric,
    nome varchar(25),
    hospital varchar(25),
    data_nascimento date,
    constraint fk_especialidade foreign key (id_especialidade) references especialidades(id_especialidade),
    constraint fk_cod_postal_medicos foreign key (cod_postal) references codigos_postais(cod_postal)
);

create table pacientes(
    id_paciente numeric primary key,
    cod_postal numeric,
    nome varchar(25),
    data_nascimento date,
    constraint fk_cod_postal_pacientes foreign key (cod_postal) references codigos_postais(cod_postal)
);

create table consultas(
    id_consulta numeric primary key,
    id_medico numeric,
    id_paciente numeric,
    data_hora date,
    constraint fk_medico_consultas foreign key (id_medico) references medicos(id_medico),
    constraint fk_paciente_consultas foreign key (id_paciente) references pacientes(id_paciente)
);

create table medicamentos_receitados(
    id_medicamento numeric,
    id_consulta numeric,
    constraint fk_medicamento_receitados foreign key (id_medicamento) references medicamentos(id_medicamento),
    constraint fk_consulta_receitados foreign key (id_consulta) references consultas(id_consulta)
);

alter table medicos add constraint fk_medico_chefe foreign key (id_medico_chefe) references medicos(id_medico);
alter table medicamentos modify (laboratorio char(15));
alter table medicamentos add constraint ck_laboratorio check (laboratorio like '__-%');
alter table medicamentos modify (nome varchar(25) not null);
alter table medicamentos modify (preco numeric(5, 2));
alter table medicamentos add constraint ck_preco_medicamentos check (preco > 30); 

--preencher tabelas

insert into especialidades values(1, 'Oncologia');
insert into especialidades values(2, 'Ginecologia');
insert into especialidades values(3, 'Urologia');
insert into especialidades values(4, 'Estomatologia');
insert into especialidades values(5, 'Psiquiatria');

insert into codigos_postais values(4000, 'Porto');
insert into codigos_postais values(1000, 'Lisboa');
insert into codigos_postais values(6000, 'Castelo Branco');
insert into codigos_postais values(9000, 'Funchal');
insert into codigos_postais values(9600, 'Azores');

insert into medicamentos values(11, 'Benetron', '11-Isep-Technol', 31.00);
insert into medicamentos values(17, 'Brufen', '17-Pfizer Biotc', 50.50);
insert into medicamentos values(13, 'Aspirina', '13-Bayern Munch', 110.50);
insert into medicamentos values(14, 'Parasetamol', '14-Mylan Bergam', 37.25);
insert into medicamentos values(15, 'Valeriana', '15-Avo Augustin', 45.00);

insert into pacientes values(111, 6000,'Violeta', to_date('1974/04/04','yyyy/mm/dd'));
insert into pacientes values(112, 6000,'Vasco', to_date('1999/12/25','yyyy/mm/dd'));
insert into pacientes values(113, 4000,'Vanessa', to_date('1988/01/08','yyyy/mm/dd'));
insert into pacientes values(114, 4000,'Viviane', to_date('2002/11/11','yyyy/mm/dd'));
insert into pacientes values(115, 1000,'Virgilio', to_date('2007/08/17','yyyy/mm/dd'));
insert into pacientes values(116, 9000,'Vinicius', to_date('1995/10/12','yyyy/mm/dd'));
insert into pacientes values(117, 9600,'Vonia', to_date('1997/07/02','yyyy/mm/dd'));
insert into pacientes values(118, 9600,'Valente', to_date('1996/12/01','yyyy/mm/dd'));

insert into medicos values(001, 3, 001, 6000, 'Rita Pererira', 'S. Nuno', to_date('1982/03/13','yyyy/mm/dd'));
insert into medicos values(002, 1, 002, 4000, 'Pinto da Costa', 'S. João', to_date('1937/12/28','yyyy/mm/dd'));
insert into medicos values(003, 1, 002, 4000, 'Sergio', 'S. João', to_date('1974/11/15','yyyy/mm/dd'));
insert into medicos values(004, 4, 004, 1000, 'Batatinha', 'Todos os Santos', to_date('1962/01/16','yyyy/mm/dd'));
insert into medicos values(005, 4, 004, 1000, 'Companhia', 'Todos os Santos', to_date('1961/04/07','yyyy/mm/dd'));
insert into medicos values(006, 2, 006, 9000, 'Cristiano', 'S. João Jardim', to_date('1985/02/05','yyyy/mm/dd'));
insert into medicos values(007, 5, 007, 9600, 'Pauleta', 'S. Paleta', to_date('1973/04/28','yyyy/mm/dd'));
insert into medicos values(008, 1, 007, 9600, 'Henriques', 'S. Paleta', to_date('1394/03/04','yyyy/mm/dd'));

insert into consultas values(10091, 001, 111, to_date('2020/03/04','yyyy/mm/dd'));
insert into consultas values(10092, 002, 113, to_date('2020/04/12','yyyy/mm/dd'));
insert into consultas values(10093, 002, 113, to_date('2020/04/13','yyyy/mm/dd'));
insert into consultas values(10094, 003, 114, to_date('2020/11/05','yyyy/mm/dd'));
insert into consultas values(10095, 004, 115, to_date('2020/01/05','yyyy/mm/dd'));
insert into consultas values(10096, 006, 116, to_date('2020/05/17','yyyy/mm/dd'));
insert into consultas values(10097, 008, 118, to_date('2020/03/25','yyyy/mm/dd'));
insert into consultas values(10098, 005, 115, to_date('2020/01/05','yyyy/mm/dd'));

insert into medicamentos_receitados values(11, 10091);
insert into medicamentos_receitados values(17, 10091);
insert into medicamentos_receitados values(15, 10091);
insert into medicamentos_receitados values(15, 10092);
insert into medicamentos_receitados values(15, 10093);
insert into medicamentos_receitados values(13, 10093);
insert into medicamentos_receitados values(14, 10093);
insert into medicamentos_receitados values(11, 10094);
insert into medicamentos_receitados values(13, 10094);
insert into medicamentos_receitados values(15, 10095);
insert into medicamentos_receitados values(13, 10095);
insert into medicamentos_receitados values(17, 10096);
insert into medicamentos_receitados values(14, 10096);
insert into medicamentos_receitados values(15, 10097);
insert into medicamentos_receitados values(11, 10098);
insert into medicamentos_receitados values(13, 10098);