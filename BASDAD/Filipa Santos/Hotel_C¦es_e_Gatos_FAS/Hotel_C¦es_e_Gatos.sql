--Eliminar tabelas
DROP TABLE animal CASCADE CONSTRAINTS PURGE;
DROP TABLE ra�a CASCADE CONSTRAINTS PURGE;
DROP TABLE porte CASCADE CONSTRAINTS PURGE;
DROP TABLE dono CASCADE CONSTRAINTS PURGE;
DROP TABLE hotel CASCADE CONSTRAINTS PURGE;
DROP TABLE quarto CASCADE CONSTRAINTS PURGE;
DROP TABLE servi�o CASCADE CONSTRAINTS PURGE;
DROP TABLE cuidador CASCADE CONSTRAINTS PURGE;
DROP TABLE reserva CASCADE CONSTRAINTS PURGE;
DROP TABLE animal_reserva CASCADE CONSTRAINTS PURGE;
DROP TABLE servi�os_reservados CASCADE CONSTRAINTS PURGE;

--Criar tabelas
CREATE TABLE animal (
codigo_animal INTEGER CONSTRAINT pk_animal PRIMARY KEY,
nome VARCHAR(20),
data_nascimento DATE,
tem_chip NUMBER(1),
cc_dono INTEGER,
id_ra�a INTEGER,
id_porte INTEGER,
CONSTRAINT ck_animal_chip CHECK (tem_chip IN (0,1))
);

CREATE TABLE ra�a (
id_ra�a INTEGER CONSTRAINT pk_ra�a PRIMARY KEY,
designacao VARCHAR(20)
);

CREATE TABLE porte (
id_porte INTEGER CONSTRAINT pk_porte PRIMARY KEY,
designacao VARCHAR(9)
);

CREATE TABLE dono (
cc_dono VARCHAR(20),
nome VARCHAR(30) CONSTRAINT dono_nome NOT NULL,
nif CHAR(9),
telemovel CHAR(9)
);

CREATE TABLE hotel (
categoria VARCHAR(5) CONSTRAINT ck_hotel_categoria CHECK (categoria IN ('C�es,Gatos')),
localiza��o VARCHAR(20),
estrelas INTEGER
);

CREATE TABLE quarto (
id_quarto INTEGER CONSTRAINT pk_quarto PRIMARY KEY,
numero INTEGER CONSTRAINT ck_quarto_numero CHECK (numero BETWEEN 1 AND 20),
capacidade INTEGER CONSTRAINT ck_quarto_capacidade CHECK (capacidade BETWEEN 1 AND 2),
tipologia VARCHAR(8),
categoria VARCHAR(5),
localiza��o VARCHAR(20),
id_tipologia INTEGER,
CONSTRAINT ck_quarto_tipologia CHECK (tipologia IN ('Standard', 'Premium'))
);

CREATE TABLE servi�o (
id_servi�o INTEGER CONSTRAINT pk_servi�o PRIMARY KEY,
designacao VARCHAR(20),
id_cuidador INTEGER,
CONSTRAINT fk_servi�o_cuidador FOREIGN KEY (id_cuidador) REFERENCES cuidador(id_cuidador)
);

CREATE TABLE cuidador (
id_cuidador INTEGER CONSTRAINT pk_cuidador PRIMARY KEY,
cc_cuidador VARCHAR(20) CONSTRAINT un_cuidador_cc UNIQUE,
nome VARCHAR(30) CONSTRAINT cuidador_nome NOT NULL,
data_nascimento DATE,
data_contrato DATE CONSTRAINT cuidador_data_contrato NOT NULL,
salario INTEGER CONSTRAINT cuidador_salario NOT NULL,
categoria VARCHAR(5),
localiza��o VARCHAR(20)
);

CREATE TABLE reserva (
id_reserva INTEGER CONSTRAINT pk_reserva PRIMARY KEY,
categoria VARCHAR(5),
localiza��o VARCHAR(20),
inicio_estadia DATE,
fim_estadia DATE,
valor_total INTEGER
);

CREATE TABLE animal_reserva(
codigo_animal INTEGER, 
id_reserva INTEGER,
id_quarto INTEGER,
CONSTRAINT pk_animal_reserva PRIMARY KEY (codigo_animal, id_reserva),
CONSTRAINT fk_animal_reserva_animal FOREIGN KEY (codigo_animal) REFERENCES animal(codigo_animal),
CONSTRAINT fk_animal_reserva_reserva FOREIGN KEY (id_reserva) REFERENCES reserva(id_reserva),
CONSTRAINT fk_animal_reserva_quarto FOREIGN KEY (id_quarto) REFERENCES quarto(id_quarto)
);

CREATE TABLE servi�os_reservados(
id_servi�o INTEGER,
codigo_animal INTEGER,
id_reserva INTEGER,
CONSTRAINT pk_servi�os_reservados PRIMARY KEY (id_servi�o, codigo_animal, id_reserva),
CONSTRAINT fk_servi�os_reservados_servi�o FOREIGN KEY (id_servi�o) REFERENCES servi�o (id_servi�o),
CONSTRAINT fk_servi�os_reservados_animal_reserva FOREIGN KEY (codigo_animal, id_reserva) REFERENCES animal_reserva (codigo_animal, id_reserva)
);

