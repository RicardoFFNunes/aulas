--Eliminar tabelas
DROP TABLE animal CASCADE CONSTRAINTS PURGE;
DROP TABLE ra�a CASCADE CONSTRAINTS PURGE;
DROP TABLE porte CASCADE CONSTRAINTS PURGE;
DROP TABLE dono CASCADE CONSTRAINTS PURGE;
DROP TABLE hotel CASCADE CONSTRAINTS PURGE;
DROP TABLE quarto CASCADE CONSTRAINTS PURGE;
DROP TABLE servi�o CASCADE CONSTRAINTS PURGE;
DROP TABLE cuidador CASCADE CONSTRAINTS PURGE;
DROP TABLE reserva CASCADE CONSTRAINTS PURGE;
DROP TABLE animal_reserva CASCADE CONSTRAINTS PURGE;
DROP TABLE servi�os_reservados CASCADE CONSTRAINTS PURGE;

--Criar tabelas
CREATE TABLE animal (
codigo_animal INTEGER CONSTRAINT pk_animal PRIMARY KEY,
nome VARCHAR(20),
data_nascimento DATE,
tem_chip NUMBER(1),
cc_dono INTEGER,
id_ra�a INTEGER,
id_porte INTEGER,
CONSTRAINT ck_animal_chip CHECK (tem_chip IN (0,1))
);

CREATE TABLE ra�a (
id_ra�a INTEGER CONSTRAINT pk_ra�a PRIMARY KEY,
designacao VARCHAR(20)
);

CREATE TABLE porte (
id_porte INTEGER CONSTRAINT pk_porte PRIMARY KEY,
designacao VARCHAR(9)
);

CREATE TABLE dono (
cc_dono VARCHAR(20),
nome VARCHAR(30) CONSTRAINT dono_nome NOT NULL,
nif CHAR(9),
telemovel CHAR(9)
);

CREATE TABLE hotel (
categoria VARCHAR(5) CONSTRAINT ck_hotel_categoria CHECK (categoria IN ('C�es,Gatos')),
localiza��o VARCHAR(20),
estrelas INTEGER
);

CREATE TABLE quarto (
id_quarto INTEGER CONSTRAINT pk_quarto PRIMARY KEY,
numero INTEGER CONSTRAINT ck_quarto_numero CHECK (numero BETWEEN 1 AND 20),
capacidade INTEGER CONSTRAINT ck_quarto_capacidade CHECK (capacidade BETWEEN 1 AND 2),
tipologia VARCHAR(8),
categoria VARCHAR(5),
localiza��o VARCHAR(20),
id_tipologia INTEGER,
CONSTRAINT ck_quarto_tipologia CHECK (tipologia IN ('Standard', 'Premium'))
);

CREATE TABLE cuidador (
id_cuidador INTEGER CONSTRAINT pk_cuidador PRIMARY KEY,
cc_cuidador VARCHAR(20) CONSTRAINT un_cuidador_cc UNIQUE,
nome VARCHAR(30) CONSTRAINT cuidador_nome NOT NULL,
data_nascimento DATE,
data_contrato DATE CONSTRAINT cuidador_data_contrato NOT NULL,
salario INTEGER CONSTRAINT cuidador_salario NOT NULL,
categoria VARCHAR(5),
localiza��o VARCHAR(20)
);

CREATE TABLE servi�o (
id_servi�o INTEGER CONSTRAINT pk_servi�o PRIMARY KEY,
designacao VARCHAR(20),
id_cuidador INTEGER,
CONSTRAINT fk_servi�o_cuidador FOREIGN KEY (id_cuidador) REFERENCES cuidador(id_cuidador)
);

CREATE TABLE reserva (
id_reserva INTEGER CONSTRAINT pk_reserva PRIMARY KEY,
categoria VARCHAR(5),
localiza��o VARCHAR(20),
inicio_estadia DATE, --timestamp
fim_estadia DATE,--timestamp
valor_total INTEGER
);

CREATE TABLE animal_reserva(
codigo_animal INTEGER, 
id_reserva INTEGER,
id_quarto INTEGER,
CONSTRAINT pk_animal_reserva PRIMARY KEY (codigo_animal, id_reserva),
CONSTRAINT fk_animal_reserva_animal FOREIGN KEY (codigo_animal) REFERENCES animal(codigo_animal),
CONSTRAINT fk_animal_reserva_reserva FOREIGN KEY (id_reserva) REFERENCES reserva(id_reserva),
CONSTRAINT fk_animal_reserva_quarto FOREIGN KEY (id_quarto) REFERENCES quarto(id_quarto)
);

CREATE TABLE servi�os_reservados(
id_servi�o INTEGER,
codigo_animal INTEGER,
id_reserva INTEGER,
CONSTRAINT pk_servi�os_reservados PRIMARY KEY (id_servi�o, codigo_animal, id_reserva),
CONSTRAINT fk_servi�os_reservados_servi�o FOREIGN KEY (id_servi�o) REFERENCES servi�o (id_servi�o),
CONSTRAINT fk_servi�os_reservados_animal_reserva FOREIGN KEY (codigo_animal, id_reserva) REFERENCES animal_reserva (codigo_animal, id_reserva)
);

--Ex.1: Alterar tabelas:
--1-a) Adicionar foreign keys na tabela Animal com base no modelo relacional (diagrama).

ALTER TABLE animal
ADD CONSTRAINT fk_animal_ra�a FOREIGN KEY (id_ra�a) REFERENCES ra�a(id_ra�a);
ALTER TABLE animal
ADD CONSTRAINT fk_animal_porte FOREIGN KEY (id_porte) REFERENCES porte(id_porte);

ALTER TABLE animal
MODIFY cc_dono CHAR(13);

ALTER TABLE dono
MODIFY cc_dono CHAR(13);

ALTER TABLE dono
ADD CONSTRAINT pk_dono PRIMARY KEY (cc_dono);
ALTER TABLE animal
ADD CONSTRAINT fk_animal_dono FOREIGN KEY (cc_dono) REFERENCES dono(cc_dono);

--1-b) Adicionar constraint na coluna "tem_chip" da tabela Animal para que tenha um valor 1 (significando true) por defeito
ALTER TABLE animal
MODIFY (tem_chip DEFAULT 1);

--1-c) Adicionar constraint na coluna "designacao" da tabela Porte para que s� possa conter um de quatro valores: Miniatura, Pequeno, M�dio e Grande
ALTER TABLE porte
ADD CONSTRAINT ck_porte_designacao CHECK (designacao IN ('Miniatura','Pequeno','M�dio','Grande'));

--1-d) Mudar o tipo de dados da coluna "cc_dono" da tabela Dono de forma a que tenha o formato de 8 d�gitos seguido de h�fen e mais 4 caracteres alfanum�ricos.
ALTER TABLE dono
MODIFY cc_dono CHAR(13);

ALTER TABLE dono
ADD CONSTRAINT ck_dono_cc CHECK (cc_dono LIKE ('________-%'));

--1-e) Fazer o mesmo da al�nea d) mas para a coluna "cc_cuidador" da tabela Cuidador.
ALTER TABLE cuidador
MODIFY cc_cuidador CHAR(13);

ALTER TABLE cuidador
ADD CONSTRAINT ck_cuidador_cc CHECK (cc_cuidador LIKE ('________-%'));

--1-f) Alterar tabela Dono de maneira a criar chaves candidatas para as colunas "nif" e "telemovel"
ALTER TABLE dono
ADD CONSTRAINT un_dono_nif UNIQUE (nif);

ALTER TABLE dono
ADD CONSTRAINT un_dono_telemovel UNIQUE (telemovel);

--1-g) Criar uma chave prim�ria composta na tabela Hotel com as colunas "categoria" e "localiza��o"
ALTER TABLE hotel
ADD CONSTRAINT pk_hotel PRIMARY KEY (categoria, localiza��o);

--1-h) Adicionar constraint na tabela Hotel, coluna "estrelas" de modo a que s� possa ter valores inteiros entre 1 e 5
ALTER TABLE hotel
ADD CONSTRAINT ck_hotel_estrelas CHECK (estrelas BETWEEN 1 AND 5);

--1-i) Adicionar chave estrangeira composta na tabela Quarto de acordo com o modelo relacional
ALTER TABLE quarto
ADD CONSTRAINT fk_quarto_hotel FOREIGN KEY (categoria, localiza��o) REFERENCES hotel (categoria, localiza��o);

--1-j) Fazer o mesmo da al�nea i) mas para as tabelas Cuidador e Reserva, de acordo com o modelo relacional
ALTER TABLE cuidador
ADD CONSTRAINT fk_cuidador_hotel FOREIGN KEY (categoria, localiza��o) REFERENCES hotel (categoria, localiza��o);

ALTER TABLE reserva
ADD CONSTRAINT fk_reserva_hotel FOREIGN KEY (categoria, localiza��o) REFERENCES hotel (categoria, localiza��o);

--1-k) Adicionar constraint na tabela Reserva de modo a que o fim da estadia seja sempre depois do seu in�cio
ALTER TABLE reserva
ADD CONSTRAINT ck_reserva_datas CHECK (fim_estadia > inicio_estadia);
--DAY(data2) - DAY(data1) > 0

--1-l) Mudar o tipo de dados da coluna "valor_total" da tabela Reserva de modo a que permita 2 casas decimais e um total de 7 d�gitos (incluindo casas decimais).
ALTER TABLE reserva
MODIFY valor_total NUMBER(7,2);

--2.
--Inserir linhas:
INSERT INTO ra�a VALUES (1,'Indefinida-C�o');
INSERT INTO ra�a VALUES (2,'Labrador');
INSERT INTO ra�a VALUES (3,'Golden Retriever');
INSERT INTO ra�a VALUES (4,'Buldog franc�s');
INSERT INTO ra�a VALUES (5,'Chihuaha');
INSERT INTO ra�a VALUES (6,'Beagle');
INSERT INTO ra�a VALUES (7,'Pinscher');
INSERT INTO ra�a VALUES (8,'Doberman');
INSERT INTO ra�a VALUES (9,'Caniche');
INSERT INTO ra�a VALUES (10,'Indefinida-Gato');
INSERT INTO ra�a VALUES (11,'Persa');
INSERT INTO ra�a VALUES (12,'Maine Coon');
INSERT INTO ra�a VALUES (13,'Gato comum europeu');
INSERT INTO ra�a VALUES (14,'Sphynx');
INSERT INTO ra�a VALUES (15,'Siam�s');
INSERT INTO ra�a VALUES (16,'Oriental short hair');
INSERT INTO ra�a VALUES (17,'Ragdoll');

INSERT INTO porte VALUES (1,'Miniatura');
INSERT INTO porte VALUES (2,'Pequeno');
INSERT INTO porte VALUES (3,'M�dio');
INSERT INTO porte VALUES (4,'Grande');

INSERT INTO dono VALUES ('11354888-5ZZ5','Rosa Reis',266598152,922556698);
INSERT INTO dono VALUES ('11324833-3ZZ6','Paulo Baltarejo',551698561,966556698);
INSERT INTO dono VALUES ('21354888-6ZZ5','�ngelo Martins',266598162,912856698);
INSERT INTO dono VALUES ('11351238-9ZY5','Paulo Maio',231551652,936556448);
INSERT INTO dono VALUES ('16154888-5ZY9','Nuno Bettencourt',287598469,913666651);
INSERT INTO dono VALUES ('13566984-2ZZ9','Filipa Santos',266594115,969988551);
INSERT INTO dono VALUES ('13566594-1ZZ8','Ricardo Nogueira',231551115,939655951);
INSERT INTO dono VALUES ('13569284-2ZZ9','Filipa Borges',215548890,936699987);
INSERT INTO dono VALUES ('13669883-2ZZ9','Raquel Camacho',255568669,932266698);
INSERT INTO dono VALUES ('13266983-6ZY9','Ricardo Mendes',198594115,926698551);
INSERT INTO dono VALUES ('15566313-9ZY7','Ricardo Batista',199894165,926987556);
INSERT INTO dono VALUES ('13566900-2ZZ9','Nuno Casteleira',269999985,936645889);

INSERT INTO animal VALUES (553668,'Bolinhas',TO_DATE('2014/04/04','yyyy/mm/dd'),1,'11354888-5ZZ5',4,2);
INSERT INTO animal VALUES (155986,'Rocky',TO_DATE('2019/11/08','yyyy/mm/dd'),1,'11354888-5ZZ5',10,2);
INSERT INTO animal VALUES (569988,'Jack',TO_DATE('2018/07/25','yyyy/mm/dd'),0,'11324833-3ZZ6',3,3);
INSERT INTO animal VALUES (456691,'Lucy',TO_DATE('216/08/12','yyyy/mm/dd'),1,'21354888-6ZZ5',5,1);
INSERT INTO animal VALUES (512569,'Nikita',TO_DATE('2012/05/03','yyyy/mm/dd'),1,'21354888-6ZZ5',15,2);
INSERT INTO animal VALUES (988715,'Kiko',TO_DATE('2019/04/25','yyyy/mm/dd'),0,'11351238-9ZY5',6,3);
INSERT INTO animal VALUES (556987,'Paolo',TO_DATE('2014/12/09','yyyy/mm/dd'),1,'11351238-9ZY5',8,4);
INSERT INTO animal VALUES (477871,'Betty',TO_DATE('2020/01/04','yyyy/mm/dd'),DEFAULT,'16154888-5ZY9',11,3);
INSERT INTO animal VALUES (477889,'Belly',TO_DATE('2018/04/15','yyyy/mm/dd'),1,'16154888-5ZY9',16,3);
INSERT INTO animal VALUES (587998,'Benny',TO_DATE('2016/02/14','yyyy/mm/dd'),DEFAULT,'16154888-5ZY9',11,3);
INSERT INTO animal VALUES (516998,'Lolita',TO_DATE('2015/03/23','yyyy/mm/dd'),0,'13566984-2ZZ9',1,2);
INSERT INTO animal VALUES (788952,'Alvin',TO_DATE('2011/08/17','yyyy/mm/dd'),0,'13566984-2ZZ9',10,2);
INSERT INTO animal VALUES (147748,'Cookie',TO_DATE('2014/06/29','yyyy/mm/dd'),0,'13566984-2ZZ9',7,1);
INSERT INTO animal VALUES (669987,'Joca',TO_DATE('2014/05/18','yyyy/mm/dd'),1,'13566594-1ZZ8',2,3);
INSERT INTO animal VALUES (225447,'Micas',TO_DATE('2019/09/20','yyyy/mm/dd'),1,'13566594-1ZZ8',12,3);
INSERT INTO animal VALUES (445568,'Suzy',TO_DATE('2009/12/17','yyyy/mm/dd'),0,'13569284-2ZZ9',2,3);
INSERT INTO animal VALUES (889668,'Simba',TO_DATE('2014/06/11','yyyy/mm/dd'),1,'13669883-2ZZ9',10,2);
INSERT INTO animal VALUES (156668,'Luke',TO_DATE('2014/08/11','yyyy/mm/dd'),1,'13669883-2ZZ9',13,2);
INSERT INTO animal VALUES (559987,'Lily',TO_DATE('2013/07/05','yyyy/mm/dd'),0,'13266983-6ZY9',3,3);
INSERT INTO animal VALUES (476626,'Hulk',TO_DATE('2013/09/05','yyyy/mm/dd'),1,'15566313-9ZY7',8,4);
INSERT INTO animal VALUES (488962,'Mike',TO_DATE('2018/04/05','yyyy/mm/dd'),1,'15566313-9ZY7',4,2);
INSERT INTO animal VALUES (478995,'Sonic',TO_DATE('2018/02/17','yyyy/mm/dd'),0,'13566900-2ZZ9',13,2);
INSERT INTO animal VALUES (478998,'Skywalker',TO_DATE('2019/12/14','yyyy/mm/dd'),0,'13566900-2ZZ9',1,3);

ALTER TABLE hotel
DROP CONSTRAINT ck_hotel_categoria;
ALTER TABLE hotel 
ADD CONSTRAINT ck_hotel_categoria CHECK (categoria IN ('C�es','Gatos'));
INSERT INTO hotel VALUES ('C�es','Porto',5);
INSERT INTO hotel VALUES ('C�es','Lisboa',4);
INSERT INTO hotel VALUES ('Gatos','Porto',5);
INSERT INTO hotel VALUES ('Gatos','Lisboa',4);

INSERT INTO cuidador VALUES (1,'11299428-1ZZ7','Ricardo Nunes', TO_DATE('1991/12/28','yyyy/mm/dd'),TO_DATE('2019/05/14','yyyy/mm/dd'),1500,'C�es','Porto');
INSERT INTO cuidador VALUES (2,'16695518-1ZZ8','Tom�s Osswald', TO_DATE('1992/07/19','yyyy/mm/dd'),TO_DATE('2020/02/01','yyyy/mm/dd'),1750,'C�es','Porto');
INSERT INTO cuidador VALUES (3,'11519848-2ZZ1','Raquel Ribeiro', TO_DATE('1990/04/05','yyyy/mm/dd'),TO_DATE('2019/09/02','yyyy/mm/dd'),1500,'C�es','Porto');
INSERT INTO cuidador VALUES (4,'13321358-1ZY8','M�rcia Guedes', TO_DATE('1980/05/29','yyyy/mm/dd'),TO_DATE('2015/06/01','yyyy/mm/dd'),2000,'Gatos','Porto');
INSERT INTO cuidador VALUES (5,'17789621-1ZZ9','Marta In�cio', TO_DATE('1991/11/28','yyyy/mm/dd'),TO_DATE('2016/08/07','yyyy/mm/dd'),1600,'Gatos','Porto');
INSERT INTO cuidador VALUES (6,'11445669-2ZZ3','Thayane Marcelino', TO_DATE('1990/03/08','yyyy/mm/dd'),TO_DATE('2018/05/01','yyyy/mm/dd'),1500,'C�es','Lisboa');
INSERT INTO cuidador VALUES (7,'15558951-2ZZ7','B�rbara Sousa', TO_DATE('1991/06/17','yyyy/mm/dd'),TO_DATE('2019/02/01','yyyy/mm/dd'),1750,'C�es','Lisboa');
INSERT INTO cuidador VALUES (8,'26668856-6ZY1','Lu�s Rodrigues', TO_DATE('1989/08/24','yyyy/mm/dd'),TO_DATE('2019/04/26','yyyy/mm/dd'),1500,'C�es','Lisboa');
INSERT INTO cuidador VALUES (9,'15554889-1ZZ6','Jo�o Pinto', TO_DATE('1986/07/29','yyyy/mm/dd'),TO_DATE('2017/09/01','yyyy/mm/dd'),2000,'Gatos','Lisboa');
INSERT INTO cuidador VALUES (10,'11566994-1ZZ9','Lu�s Belo', TO_DATE('1993/11/04','yyyy/mm/dd'),TO_DATE('2020/04/07','yyyy/mm/dd'),1600,'Gatos','Lisboa');

INSERT INTO servi�o VALUES (1,'Tosquia c�es Porto',1);
INSERT INTO servi�o VALUES (2,'Tosquia gatos Porto',4);
INSERT INTO servi�o VALUES (3,'Passeio pela natureza Porto',2);
INSERT INTO servi�o VALUES (4,'Banho c�es Porto',3);
INSERT INTO servi�o VALUES (5,'Banho gatos Porto',4);
INSERT INTO servi�o VALUES (6,'Brincar c�es Porto',3);
INSERT INTO servi�o VALUES (7,'Brincar gatos Porto',5);
INSERT INTO servi�o VALUES (8,'Tosquia c�es Lisboa',8);
INSERT INTO servi�o VALUES (9,'Tosquia gatos Lisboa',9);
INSERT INTO servi�o VALUES (11,'Banho c�es Lisboa',6);
INSERT INTO servi�o VALUES (12,'Banho gatos Lisboa',10);
INSERT INTO servi�o VALUES (13,'Brincar c�es Lisboa',7);
INSERT INTO servi�o VALUES (14,'Brincar gatos Lisboa',10);
INSERT INTO servi�o VALUES (10,'Passeio natureza Lis',8);

ALTER TABLE quarto
DROP COLUMN id_tipologia;
INSERT INTO quarto VALUES (1,1,2,'Standard','C�es','Porto');
INSERT INTO quarto VALUES (2,2,1,'Standard','C�es','Porto');
INSERT INTO quarto VALUES (3,3,1,'Standard','C�es','Porto');
INSERT INTO quarto VALUES (4,4,1,'Premium','C�es','Porto');
INSERT INTO quarto VALUES (5,1,2,'Standard','Gatos','Porto');
INSERT INTO quarto VALUES (6,2,2,'Standard','Gatos','Porto');
INSERT INTO quarto VALUES (7,3,2,'Standard','Gatos','Porto');
INSERT INTO quarto VALUES (8,4,1,'Premium','Gatos','Porto');
INSERT INTO quarto VALUES (9,1,2,'Standard','C�es','Lisboa');
INSERT INTO quarto VALUES (10,2,1,'Standard','C�es','Lisboa');
INSERT INTO quarto VALUES (11,3,1,'Standard','C�es','Lisboa');
INSERT INTO quarto VALUES (12,1,2,'Standard','Gatos','Porto'); --alterar localiza��o
INSERT INTO quarto VALUES (13,2,2,'Standard','Gatos','Porto'); --alterar localiza��o
INSERT INTO quarto VALUES (14,3,2,'Standard','Gatos','Porto'); --alterar localiza��o

INSERT INTO reserva VALUES (1,'C�es','Porto', TO_DATE('2015/05/04','yyyy/mm/dd'), TO_DATE('2015/05/16','yyyy/mm/dd'),105.60);
INSERT INTO reserva VALUES (2,'C�es','Porto', TO_DATE('2015/06/04','yyyy/mm/dd'), TO_DATE('2015/06/08','yyyy/mm/dd'),250.00);
INSERT INTO reserva VALUES (3,'C�es','Porto', TO_DATE('2016/04/25','yyyy/mm/dd'), TO_DATE('2016/05/03','yyyy/mm/dd'),5000.50);
INSERT INTO reserva VALUES (4,'C�es','Porto', TO_DATE('2017/08/02','yyyy/mm/dd'), TO_DATE('2017/08/22','yyyy/mm/dd'),800.00);
INSERT INTO reserva VALUES (5,'C�es','Porto', TO_DATE('2017/08/04','yyyy/mm/dd'), TO_DATE('2017/08/30','yyyy/mm/dd'),1050.00);
INSERT INTO reserva VALUES (6,'C�es','Porto', TO_DATE('2019/06/22','yyyy/mm/dd'), TO_DATE('2019/06/28','yyyy/mm/dd'),250.00);
INSERT INTO reserva VALUES (7,'C�es','Porto', TO_DATE('2019/04/14','yyyy/mm/dd'), TO_DATE('2019/04/30','yyyy/mm/dd'),470.50);
INSERT INTO reserva VALUES (8,'C�es','Porto', TO_DATE('2019/08/02','yyyy/mm/dd'), TO_DATE('2019/08/30','yyyy/mm/dd'),950.26);
INSERT INTO reserva VALUES (9,'C�es','Porto', TO_DATE('2019/11/05','yyyy/mm/dd'), TO_DATE('2019/11/15','yyyy/mm/dd'),1150.00);
INSERT INTO reserva VALUES (10,'C�es','Porto', TO_DATE('2020/02/01','yyyy/mm/dd'), TO_DATE('2020/02/05','yyyy/mm/dd'),355.00);
INSERT INTO reserva VALUES (11,'C�es','Lisboa', TO_DATE('2016/05/04','yyyy/mm/dd'), TO_DATE('2016/05/16','yyyy/mm/dd'),155.60);
INSERT INTO reserva VALUES (12,'C�es','Lisboa', TO_DATE('2016/06/04','yyyy/mm/dd'), TO_DATE('2016/06/08','yyyy/mm/dd'),202.00);
INSERT INTO reserva VALUES (13,'C�es','Lisboa', TO_DATE('2017/04/25','yyyy/mm/dd'), TO_DATE('2017/05/03','yyyy/mm/dd'),4500.50);
INSERT INTO reserva VALUES (14,'C�es','Lisboa', TO_DATE('2018/08/02','yyyy/mm/dd'), TO_DATE('2018/08/22','yyyy/mm/dd'),5202.00);
INSERT INTO reserva VALUES (15,'C�es','Lisboa', TO_DATE('2018/08/04','yyyy/mm/dd'), TO_DATE('2018/08/30','yyyy/mm/dd'),1150.00);
INSERT INTO reserva VALUES (16,'C�es','Lisboa', TO_DATE('2020/06/22','yyyy/mm/dd'), TO_DATE('2020/06/28','yyyy/mm/dd'),150.00);
INSERT INTO reserva VALUES (17,'C�es','Lisboa', TO_DATE('2020/04/14','yyyy/mm/dd'), TO_DATE('2020/04/30','yyyy/mm/dd'),650.50);
INSERT INTO reserva VALUES (18,'C�es','Lisboa', TO_DATE('2020/08/02','yyyy/mm/dd'), TO_DATE('2020/08/30','yyyy/mm/dd'),1000.20);
INSERT INTO reserva VALUES (19,'Gatos','Porto', TO_DATE('2015/04/04','yyyy/mm/dd'), TO_DATE('2015/04/16','yyyy/mm/dd'),135.60);
INSERT INTO reserva VALUES (20,'Gatos','Porto', TO_DATE('2015/07/04','yyyy/mm/dd'), TO_DATE('2015/07/08','yyyy/mm/dd'),300.00);
INSERT INTO reserva VALUES (21,'Gatos','Porto', TO_DATE('2016/08/25','yyyy/mm/dd'), TO_DATE('2016/09/03','yyyy/mm/dd'),8000.50);
INSERT INTO reserva VALUES (22,'Gatos','Porto', TO_DATE('2017/08/02','yyyy/mm/dd'), TO_DATE('2017/08/15','yyyy/mm/dd'),855.00);
INSERT INTO reserva VALUES (23,'Gatos','Porto', TO_DATE('2017/07/04','yyyy/mm/dd'), TO_DATE('2017/08/30','yyyy/mm/dd'),16666.00);
INSERT INTO reserva VALUES (24,'Gatos','Porto', TO_DATE('2020/05/22','yyyy/mm/dd'), TO_DATE('2020/05/28','yyyy/mm/dd'),1150.00);
INSERT INTO reserva VALUES (25,'Gatos','Lisboa', TO_DATE('2016/04/04','yyyy/mm/dd'), TO_DATE('2016/04/16','yyyy/mm/dd'),450.00);
INSERT INTO reserva VALUES (26,'Gatos','Lisboa', TO_DATE('2017/07/04','yyyy/mm/dd'), TO_DATE('2017/07/08','yyyy/mm/dd'),350.00);
INSERT INTO reserva VALUES (27,'Gatos','Lisboa', TO_DATE('2018/08/25','yyyy/mm/dd'), TO_DATE('2018/09/03','yyyy/mm/dd'),8050.50);
INSERT INTO reserva VALUES (28,'Gatos','Lisboa', TO_DATE('2018/08/02','yyyy/mm/dd'), TO_DATE('2018/08/15','yyyy/mm/dd'),555.00);
INSERT INTO reserva VALUES (29,'Gatos','Lisboa', TO_DATE('2018/07/04','yyyy/mm/dd'), TO_DATE('2018/08/30','yyyy/mm/dd'),16040.00);
INSERT INTO reserva VALUES (30,'Gatos','Lisboa', TO_DATE('2020/06/22','yyyy/mm/dd'), TO_DATE('2020/06/28','yyyy/mm/dd'),1055.00);

INSERT INTO animal_reserva VALUES (553668,1,1);
INSERT INTO animal_reserva VALUES (553668,10,2);
INSERT INTO animal_reserva VALUES (155986,19,6);
INSERT INTO animal_reserva VALUES (155986,25,12);
INSERT INTO animal_reserva VALUES (569988,4,4);
INSERT INTO animal_reserva VALUES (456691,5,2);
INSERT INTO animal_reserva VALUES (512569,20,6);
INSERT INTO animal_reserva VALUES (512569,24,7);
INSERT INTO animal_reserva VALUES (988715,6,3);
INSERT INTO animal_reserva VALUES (556987,6,3);
INSERT INTO animal_reserva VALUES (988715,9,1);
INSERT INTO animal_reserva VALUES (556987,9,1);
INSERT INTO animal_reserva VALUES (477871,19,8);
INSERT INTO animal_reserva VALUES (477889,19,7);
INSERT INTO animal_reserva VALUES (587998,19,7);
INSERT INTO animal_reserva VALUES (477871,21,6);
INSERT INTO animal_reserva VALUES (477889,21,6);
INSERT INTO animal_reserva VALUES (587998,21,5);
INSERT INTO animal_reserva VALUES (516998,2,2);
INSERT INTO animal_reserva VALUES (147748,2,2);
INSERT INTO animal_reserva VALUES (516998,3,2);
INSERT INTO animal_reserva VALUES (147748,3,2);
INSERT INTO animal_reserva VALUES (669987,7,3);
INSERT INTO animal_reserva VALUES (225447,22,6);
INSERT INTO animal_reserva VALUES (225447,30,6);
INSERT INTO animal_reserva VALUES (669987,8,2);
INSERT INTO animal_reserva VALUES (445568,11,9);
INSERT INTO animal_reserva VALUES (445568,18,10);
INSERT INTO animal_reserva VALUES (889668,27,12);
INSERT INTO animal_reserva VALUES (156668,27,12);
INSERT INTO animal_reserva VALUES (889668,28,13);
INSERT INTO animal_reserva VALUES (156668,28,13);
INSERT INTO animal_reserva VALUES (559987,12,9);
INSERT INTO animal_reserva VALUES (559987,13,9);
INSERT INTO animal_reserva VALUES (476626,14,10);
INSERT INTO animal_reserva VALUES (488962,14,11);
INSERT INTO animal_reserva VALUES (476626,15,10);
INSERT INTO animal_reserva VALUES (488962,15,11);
INSERT INTO animal_reserva VALUES (476626,16,10);
INSERT INTO animal_reserva VALUES (488962,16,11);
INSERT INTO animal_reserva VALUES (476626,17,10);
INSERT INTO animal_reserva VALUES (488962,17,11);
INSERT INTO animal_reserva VALUES (478995,26,12);
INSERT INTO animal_reserva VALUES (478995,29,14);
INSERT INTO animal_reserva VALUES (478995,23,8);

INSERT INTO servi�os_reservados VALUES (1,553668,1);
INSERT INTO servi�os_reservados VALUES (4,553668,1);
INSERT INTO servi�os_reservados VALUES (1,553668,10);
INSERT INTO servi�os_reservados VALUES (5,155986,19);
INSERT INTO servi�os_reservados VALUES (5,155986,25);
INSERT INTO servi�os_reservados VALUES (2,587998,21);
INSERT INTO servi�os_reservados VALUES (5,587998,21);
INSERT INTO servi�os_reservados VALUES (5,477889,21);
INSERT INTO servi�os_reservados VALUES (5,477871,21);
INSERT INTO servi�os_reservados VALUES (13,559987,13);
INSERT INTO servi�os_reservados VALUES(10,476626,17);
INSERT INTO servi�os_reservados VALUES(10,488962,17);
INSERT INTO servi�os_reservados VALUES(11,476626,17);
INSERT INTO servi�os_reservados VALUES(11,488962,17);
INSERT INTO servi�os_reservados VALUES (2,587998,19);


--2-a) Alterar campo da tabela �localiza��o� para Lisboa na tabela Quarto para id_quarto 12,13 e 14.
UPDATE quarto
SET localiza��o = 'Lisboa'
WHERE id_quarto IN (12,13,14);

--2-b) Apagar registos da reserva 19 associada aos animais 587998, 477871 e 477889. Nota: apagar registos nas tabelas que forem necess�rias; apenas o animal 587998 tem servi�os associados a essa reserva.
DELETE FROM servi�os_reservados sr WHERE sr.codigo_animal = 587998 AND sr.id_reserva = 19;
DELETE FROM animal_reserva ar WHERE ar.codigo_animal = 587998 AND ar.id_reserva = 19;
DELETE FROM animal_reserva ar WHERE ar.codigo_animal = 477871 AND ar.id_reserva = 19;
DELETE FROM animal_reserva ar WHERE ar.codigo_animal = 477889 AND ar.id_reserva = 19;

--2-c) Inserir uma nova reserva para o cliente de nome "Nuno Bettencourt", com os seguintes dados:
--C�digo reserva: 31
--Hotel: Gatos Porto
--In�cio estadia: 23/12/2020
--Fim estadia: 26/12/2020
--Valor total: �280
--C�digo animal: 587998
INSERT INTO reserva VALUES (31,'Gatos','Porto',TO_DATE('2020/12/23','yyyy/mm/dd'),TO_DATE('2020/12/26','yyyy/mm/dd'),280);
INSERT INTO animal_reserva VALUES (587998,31,8);
INSERT INTO servi�os_reservados VALUES (5,587998,31);

--2-d) Mostrar os campos indicados na figura. Ordenar por id_reserva.
SELECT r.id_reserva, a.nome, d.nome, r.categoria, r.localiza��o, r.inicio_estadia, r.fim_estadia, r.valor_total
FROM reserva r
INNER JOIN animal_reserva ar ON r.id_reserva = ar.id_reserva
INNER JOIN animal a ON ar.codigo_animal = a.codigo_animal
INNER JOIN dono d ON a.cc_dono = d.cc_dono
ORDER BY r.id_reserva;

--2-e) Filtrar a al�nea anterior por categoria = �C�es� e por animais come�ados pela letra B ou L. Ordenar os dados n�o por id_reserva mas por ordem alfab�tica do nome do animal.
SELECT r.id_reserva, a.nome, d.nome, r.categoria, r.localiza��o, r.inicio_estadia, r.fim_estadia, r.valor_total
FROM reserva r
INNER JOIN animal_reserva ar ON r.id_reserva = ar.id_reserva
INNER JOIN animal a ON ar.codigo_animal = a.codigo_animal
INNER JOIN dono d ON a.cc_dono = d.cc_dono
WHERE UPPER(r.categoria) LIKE 'C�ES'
AND (UPPER(a.nome) LIKE 'B%' OR LOWER(a.nome) LIKE 'l%')
ORDER BY a.nome;
--% significa alguma coisa, 0 ou nada..

--2-f) Mostrar o nome de cada dono bem como o n�mero total de reservas colocadas por si, por ordem decrescente do total de reservas. Nota: aten��o aos duplicados da tabela.
--Explicar que os duplicados se devem ao facto de id_reserva na tabela ar aparecer por cada animal que � abrangido por essa reserva.
SELECT d.nome, COUNT(DISTINCT ar.id_reserva) Total_Reservas 
FROM animal_reserva ar
INNER JOIN animal a ON ar.codigo_animal = a.codigo_animal
INNER JOIN dono d ON a.cc_dono = d.cc_dono
GROUP BY d.nome
ORDER BY Total_Reservas DESC;

SELECT d.nome, COUNT(r.id_reserva) Total_Reservas 
FROM reserva r 
INNER JOIN  animal_reserva ar ON r.id_reserva = ar.id_reserva
INNER JOIN animal a ON ar.codigo_animal = a.codigo_animal
INNER JOIN dono d ON a.cc_dono = d.cc_dono
GROUP BY d.nome
ORDER BY Total_Reservas DESC;

--2-g) Mostrar o n�mero de reservas em que cada animal aparece, bem como o nome do animal e do seu dono. Ordenar por ordem decrescente do n�mero de reservas.
SELECT a.nome, d.nome, COUNT(*) N_Reservas
FROM animal_reserva ar
INNER JOIN animal a ON ar.codigo_animal = a.codigo_animal
INNER JOIN dono d ON a.cc_dono = d.cc_dono
GROUP BY a.nome, d.nome
ORDER BY COUNT(*) DESC;

--2-h) Mostrar o n�mero de vezes que cada servi�o foi requisitado, bem como o nome do servi�o e o cuidador que o prestou. Ordenar pelo servi�o.
SELECT s.designacao, c.nome AS Cuidador, COUNT(*) AS Nr_de_Requisi��es
FROM servi�os_reservados sr
INNER JOIN servi�o s ON sr.id_servi�o = s.id_servi�o
INNER JOIN cuidador c ON s.id_cuidador = c.id_cuidador
GROUP BY s.designacao, c.nome
ORDER BY s.designacao;

--2-i) Adaptar o c�digo da al�nea anterior de forma a mostrar apenas os servi�os requisitados pelos animais do dono "Nuno Bettencourt". Deve aparecer apenas o nome de cada servi�o uma vez. Ordenar pelo nome de cada animal e, para desempate, pelo nome do servi�o.
--Explicar que Distinct � necess�rio pois aparecem duplicados consoante o n� de servi�os reservados por cada animal numa dada reserva.
SELECT DISTINCT a.nome, s.designacao, c.nome AS Cuidador
FROM servi�os_reservados sr
INNER JOIN servi�o s ON sr.id_servi�o = s.id_servi�o
INNER JOIN cuidador c ON s.id_cuidador = c.id_cuidador
INNER JOIN animal_reserva ar ON sr.codigo_animal = ar.codigo_animal AND sr.id_reserva = ar.id_reserva
INNER JOIN animal a ON a.codigo_animal = ar.codigo_animal
INNER JOIN dono d ON a.cc_dono = d.cc_dono
WHERE LOWER(d.nome) LIKE 'nuno bettencourt'
ORDER BY a.nome, s.designacao;

--2-j) Mostrar o n�mero de vezes que cada quarto �Standard� foi reservado, mas apenas os que tiveram mais que 2 reservas. Ordenar pelo id_quarto.
SELECT q.id_quarto, q.numero, q.categoria, q.localiza��o, COUNT(DISTINCT ar.id_reserva) Nr_Reservas --count de duas colunas?
FROM quarto q, animal_reserva ar
WHERE q.id_quarto = ar.id_quarto
AND UPPER(q.tipologia) LIKE 'STANDARD'
GROUP BY q.id_quarto, q.numero, q.categoria, q.localiza��o
HAVING COUNT(DISTINCT ar.id_reserva) > 2
ORDER BY 1;

--2-k) Adaptar o c�digo da al�nea anterior de modo a mostrar resultados apenas para animais de ra�a.
SELECT q.id_quarto, q.numero, q.categoria, q.localiza��o, r.designacao, COUNT(*) Nr_Reservas
FROM quarto q, animal_reserva ar, animal a, ra�a r
WHERE q.id_quarto = ar.id_quarto AND
a.codigo_animal = ar.codigo_animal AND
r.id_ra�a = a.id_ra�a AND
UPPER(q.tipologia) LIKE 'STANDARD' AND
UPPER(r.designacao) NOT LIKE 'INDEFINIDA%'
GROUP BY q.id_quarto, q.numero, q.categoria, q.localiza��o, r.designacao
HAVING COUNT(*) > 2
ORDER BY 1;

--3-a) Mostrar todos os animais e as reservas associadas a cada um, caso existam.
SELECT a.nome,ar.id_reserva
FROM animal a
LEFT OUTER JOIN animal_reserva ar ON a.codigo_animal = ar.codigo_animal
ORDER BY a.nome;

SELECT a.nome,ar.id_reserva
FROM animal_reserva ar
RIGHT OUTER JOIN animal a ON a.codigo_animal = ar.codigo_animal
ORDER BY a.nome;

--3-b) Executar o seguinte comando:
INSERT INTO animal(codigo_animal, nome, tem_chip,cc_dono,id_ra�a, id_porte) VALUES (123,'Zeca',1,'11354888-5ZZ5',1,1);
--Mostrar os animais que n�o t�m data de nascimento associada. Em vez de null apresentar data de nascimento 9999-01-01.

SELECT a.nome, nvl(a.data_nascimento, TO_DATE('1900-01-01','yyyy-mm-dd')) Data_Nascimento
FROM animal a
WHERE a.data_nascimento IS NULL;

--3.c) Mostrar os servi�os associados a cada reserva, caso existam, eliminando as repeti��es. Utilizar um Right Outer Join.
SELECT DISTINCT r.id_reserva, sr.id_servi�o, s.designacao
FROM servi�o s
INNER JOIN servi�os_reservados sr ON s.id_servi�o = sr.id_servi�o
RIGHT OUTER JOIN reserva r ON sr.id_reserva = r.id_reserva
ORDER BY r.id_reserva;

--3.d) Mostrar os gatos que desfrutaram tanto do servi�o de tosquia de gatos no Porto como de qualquer servi�o de banho.
SELECT a.nome
FROM servi�os_reservados sr 
INNER JOIN servi�o s ON sr.id_servi�o = s.id_servi�o
INNER JOIN animal_reserva ar ON sr.codigo_animal = ar.codigo_animal AND sr.id_reserva = ar.id_reserva
INNER JOIN animal a ON ar.codigo_animal = a.codigo_animal
WHERE LOWER(s.designacao) LIKE 'banho%'
INTERSECT 

SELECT a.nome
FROM servi�os_reservados sr 
INNER JOIN servi�o s ON sr.id_servi�o = s.id_servi�o
INNER JOIN animal_reserva ar ON sr.codigo_animal = ar.codigo_animal AND sr.id_reserva = ar.id_reserva
INNER JOIN animal a ON ar.codigo_animal = a.codigo_animal
WHERE LOWER(s.designacao) LIKE 'tosquia gatos porto'
INTERSECT 
SELECT a.nome
FROM servi�os_reservados sr 
INNER JOIN servi�o s ON sr.id_servi�o = s.id_servi�o
INNER JOIN animal_reserva ar ON sr.codigo_animal = ar.codigo_animal AND sr.id_reserva = ar.id_reserva
INNER JOIN animal a ON ar.codigo_animal = a.codigo_animal
WHERE LOWER(s.designacao) LIKE 'banho%';

--Ordem das interse��es interessa?

--3.e) Modificar o c�digo da al�nea anterior de modo a mostrar os animais que apenas usufruiram de qualquer servi�o de banho.
SELECT a.nome
FROM servi�os_reservados sr 
INNER JOIN servi�o s ON sr.id_servi�o = s.id_servi�o
INNER JOIN animal_reserva ar ON sr.codigo_animal = ar.codigo_animal AND sr.id_reserva = ar.id_reserva
INNER JOIN animal a ON ar.codigo_animal = a.codigo_animal
WHERE LOWER(s.designacao) LIKE 'banho%'
MINUS

SELECT a.nome
FROM servi�os_reservados sr 
INNER JOIN servi�o s ON sr.id_servi�o = s.id_servi�o
INNER JOIN animal_reserva ar ON sr.codigo_animal = ar.codigo_animal AND sr.id_reserva = ar.id_reserva
INNER JOIN animal a ON ar.codigo_animal = a.codigo_animal
WHERE LOWER(s.designacao) LIKE 'tosquia gatos porto'
MINUS
SELECT a.nome
FROM servi�os_reservados sr 
INNER JOIN servi�o s ON sr.id_servi�o = s.id_servi�o
INNER JOIN animal_reserva ar ON sr.codigo_animal = ar.codigo_animal AND sr.id_reserva = ar.id_reserva
INNER JOIN animal a ON ar.codigo_animal = a.codigo_animal
WHERE LOWER(s.designacao) LIKE 'banho%';

--3.f) Modificar o c�digo da al�nea anterior de modo a mostrar os animais que usufruiram ou de qualquer tipo de banho, ou de tosquia para gatos no Porto, ou de ambos, eliminando as repeti��es.
SELECT a.nome
FROM servi�os_reservados sr 
INNER JOIN servi�o s ON sr.id_servi�o = s.id_servi�o
INNER JOIN animal_reserva ar ON sr.codigo_animal = ar.codigo_animal AND sr.id_reserva = ar.id_reserva
INNER JOIN animal a ON ar.codigo_animal = a.codigo_animal
WHERE LOWER(s.designacao) LIKE 'banho%'
UNION
SELECT a.nome
FROM servi�os_reservados sr 
INNER JOIN servi�o s ON sr.id_servi�o = s.id_servi�o
INNER JOIN animal_reserva ar ON sr.codigo_animal = ar.codigo_animal AND sr.id_reserva = ar.id_reserva
INNER JOIN animal a ON ar.codigo_animal = a.codigo_animal
WHERE LOWER(s.designacao) LIKE 'tosquia gatos porto';

--4.a) Listar os cuidadores que recebem o sal�rio m�ximo.
SELECT c.nome
FROM cuidador c
WHERE c.salario = (SELECT MAX(salario) FROM cuidador);

--4.b) Listar os cuidadores que recebem o sal�rio m�nimo no seu hotel. Ordenar por categoria e localiza��o.
SELECT c.nome, c.categoria, c.localiza��o
FROM cuidador c
WHERE c.salario = (SELECT MIN(c2.salario) 
                   FROM cuidador c2 WHERE c2.localiza��o = c.localiza��o
                   AND c2.categoria = c.categoria)
ORDER BY c.categoria, c.localiza��o;

--4.c) Mostrar, para cada cuidador, a diferen�a do seu sal�rio para o sal�rio m�ximo do seu hotel.
SELECT c.nome, c.categoria, c.localiza��o, (c.salario-(SELECT MAX(c2.salario) 
                                            FROM cuidador c2
                                            WHERE c2.localiza��o = c.localiza��o AND
                                            c2.categoria = c.categoria)) Diferen�a
FROM cuidador c 
ORDER BY c.categoria, c.localiza��o;

--4.d) Mostrar as reservas cujo n�mero de animais � superior ao n�mero de animais m�dio por reserva.

SELECT ar.id_reserva, COUNT(*) Nr_Animais
FROM animal_reserva ar 
--INNER JOIN animal a ON ar.codigo_animal = a.codigo_animal
GROUP BY ar.id_reserva
HAVING COUNT(*) > (SELECT TRUNC(AVG(COUNT(*)),0)
                   FROM animal_reserva ar
                   --INNER JOIN animal a ON ar.codigo_animal = a.codigo_animal
                   GROUP BY ar.id_reserva)
ORDER BY 1;                   

--4.e) Mostrar o nome dos donos cujos animais nunca frequentaram nenhum servi�o, ordenando por ordem alfab�tica (usar operador NOT IN).
SELECT DISTINCT d.nome
FROM dono d
INNER JOIN animal a ON d.cc_dono = a.cc_dono
WHERE a.codigo_animal NOT IN (SELECT sr.codigo_animal
                              FROM servi�os_reservados sr)
ORDER BY 1;                             

--4.f) Mostrar os animais que ficaram alojados em quartos standard na zona do Porto (usar operador IN).

SELECT a.nome
FROM animal a
WHERE EXISTS (SELECT a2.codigo_animal
                          FROM animal a2
                          INNER JOIN animal_reserva ar ON a2.codigo_animal = ar.codigo_animal
                          INNER JOIN quarto q ON ar.id_quarto = q.id_quarto
                          WHERE UPPER(q.tipologia) LIKE 'STANDARD'
                          AND a2.codigo_animal = a.codigo_animal
                          AND UPPER(q.localiza��o) LIKE 'PORTO'); --se o exists n�o for correlacionado estamos a us�-lo mal

WHERE a.codigo_animal IN (SELECT a2.codigo_animal
                          FROM animal a2
                          INNER JOIN animal_reserva ar ON a2.codigo_animal = ar.codigo_animal
                          INNER JOIN quarto q ON ar.id_quarto = q.id_quarto
                          WHERE UPPER(q.tipologia) LIKE 'STANDARD'
                          AND UPPER(q.localiza��o) LIKE 'PORTO');
                          
--4.g) Retornar as reservas cujo valor total � superior a �2000 (usar o operador > ALL). Ordenar por valor da reserva.

SELECT r.id_reserva, r.valor_total
FROM reserva r
WHERE valor_total > ALL (SELECT r2.valor_total
                         FROM reserva r2 
                         WHERE r2.valor_total <= 2000)
ORDER BY 2; 

--4.h) Retornar os donos que fizeram reservas sempre para o mesmo n�mero de animais (usar o operador = ALL).

SELECT DISTINCT d.nome, COUNT(*) Nr_Animais
FROM dono d
INNER JOIN animal a ON d.cc_dono = a.cc_dono
INNER JOIN animal_reserva ar ON a.codigo_animal = ar.codigo_animal
GROUP BY d.nome, ar.id_reserva
HAVING COUNT(*) = ALL (SELECT COUNT(*) 
                       FROM animal_reserva ar2
                       INNER JOIN animal a2 ON ar2.codigo_animal = a2.codigo_animal
                       INNER JOIN dono d2 ON a2.cc_dono = d2.cc_dono
                       WHERE d2.nome = d.nome
                       GROUP BY ar2.id_reserva
                       )
ORDER BY 1;

--4.i) Retornar os servi�os que n�o s�o os mais reservados (usar o operador < ANY). Come�ar a listagem pelos que t�m maior n�mero de reservas.

SELECT s.designacao, COUNT(*) Nr_Requisi��es
FROM servi�os_reservados sr
INNER JOIN servi�o s ON sr.id_servi�o = s.id_servi�o
GROUP BY s.designacao
HAVING COUNT(*) < ANY (SELECT COUNT(*)
                       FROM servi�os_reservados sr2
                       INNER JOIN servi�o s2 ON sr2.id_servi�o = s2.id_servi�o
                       GROUP BY s2.designacao)
ORDER BY 2 DESC;                       

                             
--4.j) Mostrar os animais que j� ficaram em quartos Premium usando o EXISTS.
              
SELECT a.nome
FROM animal a
WHERE EXISTS (SELECT *
              FROM animal_reserva ar
              INNER JOIN quarto q ON ar.id_quarto = q.id_quarto
              WHERE LOWER(q.tipologia) LIKE 'premium' 
              AND ar.codigo_animal = a.codigo_animal);
              
--4.k) Mostrar os animais que j� ficaram hospedados em ambas as localiza��es (Porto e Lisboa), usando o EXISTS.

SELECT a.nome 
FROM animal a
WHERE EXISTS (SELECT a2.nome
              FROM animal a2
              INNER JOIN animal_reserva ar ON a2.codigo_animal = ar.codigo_animal
              INNER JOIN reserva r ON ar.id_reserva = r.id_reserva
              WHERE LOWER(r.localiza��o) LIKE 'porto'
              AND ar.codigo_animal = a.codigo_animal
              INTERSECT
              SELECT a2.nome
              FROM animal a2
              INNER JOIN animal_reserva ar ON a2.codigo_animal = ar.codigo_animal
              INNER JOIN reserva r ON ar.id_reserva = r.id_reserva
              WHERE LOWER(r.localiza��o) LIKE 'lisboa'
              AND ar.codigo_animal = a.codigo_animal);
              

--4.l) Listar os servi�os que tiveram mais do que um animal de uma determinada ra�a, usando o EXISTS.
SELECT s.designacao
FROM servi�o s
WHERE EXISTS(SELECT s2.designacao, r.designacao, COUNT(*)
                 FROM servi�o s2, servi�os_reservados sr, animal_reserva ar, animal a, ra�a r
                 WHERE s2.id_servi�o = sr.id_servi�o AND sr.id_reserva = ar.id_reserva 
                 AND sr.codigo_animal = ar.codigo_animal AND ar.codigo_animal = a.codigo_animal 
                 AND a.id_ra�a = r.id_ra�a AND s2.id_servi�o = s.id_servi�o
                 GROUP BY s2.designacao, r.designacao
                 HAVING COUNT(*) > 1);

--4.m) Mostrar os servi�os que nunca foram requisitados, usando o NOT EXISTS.
SELECT s.id_servi�o, s.designacao
FROM servi�o s
WHERE NOT EXISTS (SELECT *
                  FROM servi�os_reservados sr
                  INNER JOIN servi�o s2 ON sr.id_servi�o = s2.id_servi�o
                  WHERE s2.id_servi�o = s.id_servi�o);
              
--4.n) Mostrar os donos que s� fizeram reservas abaixo de �5000, usando o NOT EXISTS. Ordenar por ordem alfab�tica.                            
SELECT d.nome
FROM dono d
WHERE NOT EXISTS (SELECT *
                  FROM reserva r
                  INNER JOIN animal_reserva ar ON r.id_reserva = ar.id_reserva
                  INNER JOIN animal a ON ar.codigo_animal = a.codigo_animal
                  INNER JOIN dono d2 ON a.cc_dono = d2.cc_dono
                  WHERE d2.nome = d.nome
                  AND r.valor_total > 5000
                  )
ORDER BY 1;                 

--Verifica��o:
SELECT DISTINCT d.nome, r.valor_total
FROM reserva r
INNER JOIN animal_reserva ar ON r.id_reserva = ar.id_reserva
INNER JOIN animal a ON ar.codigo_animal = a.codigo_animal
INNER JOIN dono d ON a.cc_dono = d.cc_dono
WHERE r.valor_total > 5000
ORDER BY 1;

                 
